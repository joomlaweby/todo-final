<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 07/07/2019
 * Time: 20:14
 */

defined('_APP_EXEC') or die;

function deleteDirectory($str) {
    //It it's a file.
    if (is_file($str)) {
        //Attempt to delete it.
        return unlink($str);
    }
    //If it's a directory.
    elseif (is_dir($str)) {
        //Get a list of the files in this directory.
        $scan = glob(rtrim($str,'/').'/*');
        //Loop through the list of files.
        foreach($scan as $index=>$path) {
            //Call our recursive function.
            deleteDirectory($path);
        }
        //Remove the directory itself.
        return @rmdir($str);
    }
}