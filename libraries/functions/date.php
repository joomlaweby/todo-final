<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 25/07/2019
 * Time: 23:06
 */
defined('_APP_EXEC') or die;



/**
 * Counts days to required date.
 *
 * @var    date
 *  *
 * @return  int  Days
 *
 */
function countDaysleft($date) {
    $dateTimestamp = strtotime($date);
    $nowTimestamp = time();
    $result = round((int)($dateTimestamp - $nowTimestamp)/(60 * 60 * 24)) +1;
    return $result;
}