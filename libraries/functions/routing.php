<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 12/07/2019
 * Time: 20:11
 */

/**
 * Get Segments
 *
 * From a url like http://example.com/edit/5
 * it creates an array of URI segments [ edit, 5 ]
 *
 * @return array
 */

defined('_APP_EXEC') or die;

function getSegments()
{
    $current_url = 'http' .
        ( isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 's://' : '://' ) .
        $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

    $path = str_replace( BASE_URL, '', $current_url );
    $path = trim(parse_url( $path, PHP_URL_PATH ), '/');

    $segments = explode( '/', $path );
    return $segments;
}



/**
 * Segment
 *
 * Returns the $index-th URI segment
 *
 * @param $index
 * @return string or false
 */
function segment( $index )
{
    $segments = getSegments();
    return isset( $segments[ $index-1 ] ) ? $segments[ $index-1 ] : false;
}