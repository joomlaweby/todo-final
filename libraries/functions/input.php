<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 07/07/2019
 * Time: 23:18
 */

defined('_APP_EXEC') or die;

function isPostId () {
    if (isset($_POST['id'])
        and is_numeric($_POST['id']))
    {
        return true;
    }
    else {
        return false;
    }
}

function is_ajax() {
    return (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
}

function isValidGetId ($id) {
    if (isset($_GET['id'])
        and is_numeric($id))
    {
        return true;
    }
    else
    {
        return false;
    }
}

function isValidController ($operaration) {
    if ($this->isGetOperation($operaration) and $this->isValidGetId($_GET['id'])) {
        $status = true;
    }
    else {
        $status = false;
    }
    return $status;
}

function isGetController ($operation) {
    if (isset($_GET['controller']) and $_GET['controller'] ==  $operation)
    {
        $status = true;
    }
    else {
        $status = false;
    }
    return $status;
}