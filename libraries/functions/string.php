<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 07/07/2019
 * Time: 16:27
 */
defined('_APP_EXEC') or die;

function randString ($length = 1) {
    $array1 = range('a', 'z');
    $array2 = range('A', 'Z');
    $array3 = [];

    $string = '';

    for ($a = 0; $a < 10; $a++) {
        $array3[] = $a;
    }

    $array = array_merge($array3, $array1, $array2, $array3);


    for ($i = $length; $i > 0; $i--) {

        $string = $string . $array[rand(0, (count($array) - 1))];
    }

    return $string;

}

function createAlias ($string) {


    $convert_table = Array(
        'ä'=>'a',
        'Ä'=>'A',
        'á'=>'a',
        'Á'=>'A',
        'à'=>'a',
        'À'=>'A',
        'ã'=>'a',
        'Ã'=>'A',
        'â'=>'a',
        'Â'=>'A',
        'č'=>'c',
        'Č'=>'C',
        'ć'=>'c',
        'Ć'=>'C',
        'ď'=>'d',
        'Ď'=>'D',
        'ě'=>'e',
        'Ě'=>'E',
        'é'=>'e',
        'É'=>'E',
        'ë'=>'e',
        'Ë'=>'E',
        'è'=>'e',
        'È'=>'E',
        'ê'=>'e',
        'Ê'=>'E',
        'í'=>'i',
        'Í'=>'I',
        'ï'=>'i',
        'Ï'=>'I',
        'ì'=>'i',
        'Ì'=>'I',
        'î'=>'i',
        'Î'=>'I',
        'ľ'=>'l',
        'Ľ'=>'L',
        'ĺ'=>'l',
        'Ĺ'=>'L',
        'ń'=>'n',
        'Ń'=>'N',
        'ň'=>'n',
        'Ň'=>'N',
        'ñ'=>'n',
        'Ñ'=>'N',
        'ó'=>'o',
        'Ó'=>'O',
        'ö'=>'o',
        'Ö'=>'O',
        'ô'=>'o',
        'Ô'=>'O',
        'ò'=>'o',
        'Ò'=>'O',
        'õ'=>'o',
        'Õ'=>'O',
        'ő'=>'o',
        'Ő'=>'O',
        'ř'=>'r',
        'Ř'=>'R',
        'ŕ'=>'r',
        'Ŕ'=>'R',
        'š'=>'s',
        'Š'=>'S',
        'ś'=>'s',
        'Ś'=>'S',
        'ť'=>'t',
        'Ť'=>'T',
        'ú'=>'u',
        'Ú'=>'U',
        'ů'=>'u',
        'Ů'=>'U',
        'ü'=>'u',
        'Ü'=>'U',
        'ù'=>'u',
        'Ù'=>'U',
        'ũ'=>'u',
        'Ũ'=>'U',
        'û'=>'u',
        'Û'=>'U',
        'ý'=>'y',
        'Ý'=>'Y',
        'ž'=>'z',
        'Ž'=>'Z',
        'ź'=>'z',
        'Ź'=>'Z'
    );

    $modified_string = strtr($string, $convert_table);

    $modified_string = str_replace(" ","-",strtolower($modified_string));


    return $modified_string;
}

function arrayKeysToString ($array) {
    if (!is_array($array)) {
        return false;
    };
    $string = '';
    $count = count($array);
    foreach ($array as $key => $value) {
        if ($count == 1) {
            $string .= $key;
        }
        else {
            $string .= $key . ', ';
        }

        $count --;
    }
    return $string;
}

function keysToDbColumns ($array) {
    if (!is_array($array)) {
        return false;
    };

    $new_array = [];

    foreach ($array as $key => $value) {

    }
}

