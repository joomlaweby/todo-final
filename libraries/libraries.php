<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 07/07/2019
 * Time: 16:25
 */

defined('_APP_EXEC') or die;


require_once 'functions/String.php';
require_once 'functions/File.php';
require_once 'functions/Input.php';
require_once 'functions/Routing.php';
require_once 'functions/Date.php';

