<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 22/07/2019
 * Time: 21:42
 */



define('_APP_EXEC', 1);
define('_APP_ADMIN', 1);
define ('_APP_LOC', $_SERVER['DOCUMENT_ROOT']);


//  var_dump($_GET); exit;

// var_dump($_SESSION);


error_reporting(E_ALL & ~E_NOTICE);

require_once("../inc/Defines.php");
require_once '../libraries/vendor/autoload.php';
require_once("../libraries/libraries.php");

try {

    $container = (new Joomla\DI\Container)
        ->registerServiceProvider(new App\Service\ApplicationProvider)
        ->registerServiceProvider(new App\Service\ConfigServiceProvider(JPATH_CONFIGURATION . '/config.json'))
        ->registerServiceProvider(new App\Service\DatabaseServiceProvider)
        ->registerServiceProvider(new App\Service\EventProvider)
        ->registerServiceProvider(new App\Service\HttpProvider)
        ->registerServiceProvider(new App\Service\LoggingProvider)
        ->registerServiceProvider(new Joomla\Preload\Service\PreloadProvider)
        ->registerServiceProvider(new App\Service\TemplatingProvider);

    // Alias the web logger to the PSR-3 interface as this is the primary logger for the environment
    $container->alias(Monolog\Logger::class, 'monolog.logger.application.web')
        ->alias(Psr\Log\LoggerInterface::class, 'monolog.logger.application.web');

    // Alias the web application to Joomla's base application class as this is the primary application for the environment
    $container->alias(Joomla\Application\AbstractApplication::class, Joomla\Application\AbstractWebApplication::class);

    $app = new App\AdminApplication($container);


// Execute the application.
    $app->execute();


} catch (Exception $e) {
    App\WebApplication::alertMessage('Caught exception: ' .  $e->getMessage() . "\n");
}