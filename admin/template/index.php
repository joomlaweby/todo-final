<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 13/07/2019
 * Time: 00:45
 */

defined('_APP_EXEC') or die;
include_once _APP_LOC . "/admin/template/layouts/header.php";

    $config = $this->getConfig();

?>


<main>
    <div class="container">
        <header class="border-bottom border-dark">
            <span class="pull-right"><a href="<?php echo $config->get('site_url')?>" ><i class="glyphicon glyphicon-share"></i>&nbsp;View site</a></span>
            <h3><strong>Administration</strong></h3>
            <?php include_once _APP_LOC . '/admin/template/layouts/main_menu.php'; ?>

        </header>

        <?php

        \App\WebApplication::displaySystemMessages();

        \App\WebApplication::getView($view, $view_items, $view_formdata);



        ?>




    </div>

</main>

<?php include_once _APP_LOC . "/admin/template/layouts/footer.php"; ?>
