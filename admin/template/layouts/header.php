<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 14/07/2019
 * Time: 11:35
 */

defined('_APP_EXEC') or die;


$params = json_decode($user_settings['params']);

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
    <title>TODO List administration</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="/libraries/node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" id="theme" type="text/css">
    <link rel="stylesheet" href="/admin/template/css/glyphicons.css" type="text/css">
    <link rel="stylesheet" href="/admin/template/css/main.css?v=20190812" type="text/css">
    <link rel="stylesheet" href="/libraries/vendor/harvesthq/chosen/chosen.min.css" type="text/css">
    <link rel="stylesheet" href="/libraries/node_modules/quill/dist/quill.snow.css" type="text/css">
    <script src="/admin/template/js/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="/libraries/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="/admin/template/js/app.js"></script>
    <script src="/libraries/vendor/harvesthq/chosen/chosen.jquery.min.js"></script>
    <script src="/libraries/node_modules/quill/dist/quill.js"></script>


</head>
