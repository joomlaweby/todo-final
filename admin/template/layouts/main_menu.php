<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 14/07/2019
 * Time: 11:41
 */

defined('_APP_EXEC') or die;
?>

    <nav class="navbar navbar-expand-lg">

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/config/get">
                        Settings
                    </a>

                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/user/getallusers">Users</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="/menu/getallitems">
                        Menu
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="/category/getallitems">Categories</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="/blog/getallitems">Articles</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="/tag/getallitems">Tags</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="/page/getallitems">Pages</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="/category/getallitems">Comments</a>
                </li>


            </ul>
            <form class="form-inline my-2 my-lg-0">
                <p class="pull-right">Hello, <?php echo $_SESSION['user']['username'] . ' '; ?><a href="/user/logoutSubmit" class="btn btn-warning">Logout</a></p>
            </form>
        </div>
    </nav>

