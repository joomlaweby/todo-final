<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 14/07/2019
 * Time: 11:36
 */

defined('_APP_EXEC') or die;

$jokeController = new App\Cms\Components\Chuck\Controller\ChuckController();
$view_joke = $jokeController->getJoke();

?>

<footer class="border-top border-light mt-4">
    <div class="container">
        <div class="row">
            <blockquote class="blockquote">
                <p class="mb-0 mt-4"><?php echo $view_joke ?></p>
            </blockquote>

        </div>

    </div>


</footer>

</body>
</html>