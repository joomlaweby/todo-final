<?php
/**
 * @copyright  Copyright (C) 2012 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace App\Admin\Components\Dashboard\View;
defined('_APP_EXEC') or die;

use Joomla\Factory;
use Joomla\Language\Text;
use Joomla\Input;
use App\View\DefaultHtmlView;
use App\Base\AppClasses\Document;
use App;
use Joomla\View\BaseHtmlView;
use Joomla\Renderer\RendererInterface;
/**
 * Dashboard HTML view class for the application
 *
 * @since  1.0
 */
class DashboardHtmlView extends BaseHtmlView
{

	/**
	 * Instantiate the view.
	 *
	 * @param   RendererInterface  $renderer          The renderer object.
	 * @param   Document $document                    Document object
	 */
	public function __construct(RendererInterface $renderer, Document $document)
	{
		parent::__construct($renderer);
		$this->document = $document;
	}
	/**
	 * Method to render the view.
	 *
	 * @return  string  The rendered view.
	 *
	 * @since   1.0
	 * @throws  \RuntimeException
	 */
	public function render()
	{

		$this->setData(
				[
						'dashboard' => ['title' => 'Dashboard'],
					    'document' => $this->document,
				]
		);

		return parent::render();
	}
}
