<?php
namespace App\Admin\Components\Users\View;
use App\Base\AppClasses\Document;
use Joomla\Renderer\RendererInterface;
use Joomla\View\BaseHtmlView;

defined('_APP_EXEC') or die;

class LoginHtmlView extends BaseHtmlView
{
    /**
     * Instantiate the view.
     *
     * @param   RendererInterface  $renderer          The renderer object.
     * @param   Document $document                    Document object
     */
    public function __construct(RendererInterface $renderer, Document $document)
    {
        parent::__construct($renderer);
        $this->document = $document;
    }
}