<?php


namespace App\Admin\Components\Modules\Controller;
defined('_APP_EXEC') or die;

use App\Admin\Components\Modules\View\ModulesHtmlView;
use App\Base\Controller\AbstractAppController;
use Joomla\Application\AbstractApplication;
use Joomla\Input\Input;
use Zend\Diactoros\Response\HtmlResponse;

class ModulesController extends AbstractAppController
{
    /**
     * The view object.
     *
     * @var  ModulesHtmlView
     */
    private $view;

    /**
     * Constructor.
     *
     * @param   ModulesHtmlView      $view   The view object.
     * @param   Input                $input  The input object.
     * @param   AbstractApplication  $app    The application object.
     */
    public function __construct(ModulesHtmlView $view, Input $input = null, AbstractApplication $app = null)
    {
        parent::__construct($input, $app);

        $this->view = $view;
    }

    /**
     * Execute the controller.
     *
     * @return  boolean
     */
    public function execute(): bool
    {
        // Enable browser caching
        $this->getApplication()->allowCache(true);

        $this->getApplication()->setResponse(new HtmlResponse($this->view->render()));

        return true;
    }
    

}