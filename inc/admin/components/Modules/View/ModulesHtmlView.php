<?php


namespace App\Admin\Components\Modules\View;

use Joomla\Factory;
use Joomla\Language\Text;
use Joomla\Input;
use App\View\DefaultHtmlView;
use App\Base\AppClasses\Document;
use App;
use Joomla\View\BaseHtmlView;
use Joomla\Renderer\RendererInterface;
use App\Admin\Components\Modules\Model\ModulesModel;
class ModulesHtmlView  extends BaseHtmlView {
    private $modulesModel;

    /**
     * Instantiate the view.
     *
     * @param   RendererInterface  $renderer          The renderer object.
     * @param   Document $document                    Document object
     * @param   ModulesModel                          Modules model
     */
    public function __construct(RendererInterface $renderer, Document $document, ModulesModel $modulesModel)
    {
        parent::__construct($renderer);
        $this->document = $document;
        $this->modulesModel = $modulesModel;
    }
    /**
     * Method to render the view.
     *
     * @return  string  The rendered view.
     *
     * @since   1.0
     * @throws  \RuntimeException
     */
    public function render()
    {

        $this->setData(
            [
                'modules' => $this->modulesModel->getAllItems(),
                'document' => $this->document,
            ]
        );

        return parent::render();
    }
}