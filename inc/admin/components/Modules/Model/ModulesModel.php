<?php


namespace App\Admin\Components\Modules\Model;
use App\Base\Model\AbstractItemModel;
use Joomla\Database\DatabaseDriver;

defined('_APP_EXEC') or die;



class ModulesModel extends AbstractItemModel
{
    function __construct(DatabaseDriver $db = null)
    {
        parent::__construct($db);
        $this->table = $this->getTableName();
    }
}