<?php

    defined('_APP_EXEC') or die;

    $params = json_decode($view_items->params);

    App\WebApplication::displaySystemMessages();

?>

<form method="post" id="settings-form" class="" action="/User/settingsSubmit">
    <h1 class="h2 mb-3 font-weight-normal">User settings</h1>

    <div class="form-group">
        <label for="settings-avatar" class="col-sm-2 col-form-label"><strong>Avatar</strong></label>
        <div class="input-group mb-2">
            <div class="input-group-prepend">
                <span class="input-group-text">Upload</span>
            </div>
            <div class="custom-file">
                <input type="file" class="custom-file-input" name="settings-avatar" id="settings-avatar">
                <label class="custom-file-label" for="settings-avatar">Choose file</label>
            </div>
        </div>
        <label for="settings-choose-theme" class="col-sm-2 col-form-label"><strong>Theme</strong></label>
        <select id="settings-choose-theme" class="form-control" name="settings-choose-theme">
            <?php for ($i = 1; $i <= 5; $i++) : ?>
            <option value="<?= $i ?>" <?= $params->style == $i ?  'selected' : ''?>>Style <?= $i ?></option>
            <?php endfor ?>
        </select>
        <label for="settings-choose-dateformat" class="col-sm-2 col-form-label"><strong>Date format</strong></label>
        <select id="settings-choose-dateformat" class="form-control" name="settings-choose-dateformat">

                <option value="Y-m-d">Y-m-d</option>
                <option value="Y.m.d">Y.m.d</option>
        </select>
    </div>
<h2>Api Key</h2>
    <pre><?= $view_items->apiKey ?></pre>
    <input type="submit" class="btn btn-lg btn-primary" value="Save">
</form>

<?php
