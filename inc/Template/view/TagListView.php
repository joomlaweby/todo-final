<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 26/07/2019
 * Time: 23:25
 */

defined('_APP_EXEC') or die;
include_once _APP_LOC . '/inc/Template/layouts/toolbar.php';
App\WebApplication::displaySystemMessages();
?>


<table class='table clearfix table-striped item-list'>

    <thead class='thead-dark'>
    <tr>
        <th>#</th>
        <th>Tag</th>
        <th>Published</th>
        <th>Created At</th>
        <th>Actions</th>
    </tr>
    </thead>

    <?php

    $lastRowIndex = count ($view_items) - 1;
    ?>
    <?php foreach ((array)$view_items as $key => $value) : ?>

        <tr><td><?php echo $value->id ?></td>
            <td><h4 class="text-primary"><?php echo $value->name ?></h4></td>

            <td>
                <?php if ($value->published == true) : ?>

                    <a href='/tag/unpublishItem?id=<?php echo $value->id ?>' class='glyphicon glyphicon-ok text-success' title='is active'></a>

                <?php endif ?>

                <?php if ($value->published == false) : ?>

                    <a href='/tag/publishItem?id=<?php echo $value->id ?>' class='glyphicon glyphicon-remove text-danger' title='is active'></a>

                <?php endif ?>
            </td>

            <td>
                <?php echo substr($value->createdAt, 0, 10) ?>
            </td>
            <td class='actions'>
                <a href='/tag/deleteItem?id=<?php echo $value->id ?>' class='glyphicon glyphicon-trash delete-link' title='Delete category'></a>
                <a href='/tag/getItem?id=<?php echo $value->id ?>' class='glyphicon glyphicon-edit edit-link' title='Edit category'></a>

                <?php if($key != 0) : ?>
                    <a href='/tag/moveUp?id=<?php echo $value->id ?>' class='glyphicon glyphicon-arrow-up' title='move up'></a>
                <?php endif ?>

                <?php if($lastRowIndex != $key) : ?>
                    <a href='/tag/moveDown?id=<?php echo $value->id ?>' class='glyphicon glyphicon-arrow-down' title='move down'></a>
                <?php endif ?>
            </td>
        </tr>
    <?php endforeach ?>
</table>