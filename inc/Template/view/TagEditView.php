<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 26/07/2019
 * Time: 23:47
 */

defined('_APP_EXEC') or die;

App\WebApplication::displaySystemMessages();
if ($view_items) {
    $action = "updateItem";
    $isEdit = true;
} else {
    $action = "insertItem";
    $isEdit = false;
}

?>

<form method="post" id="tag-item-form" class="" action="/tag/<?= $action ?>">
        <h1 class="h2 mb-3 font-weight-normal">Tag</h1>
        <label for="name" class="col-sm-2 col-form-label"><strong>Name</strong></label>
        <input type="text" class="form-control" name="name" value="<?= isset($view_items) ? $view_items->name : ''?>">
        <label for="alias" class="col-sm-2 col-form-label"><strong>Alias</strong></label>
        <input type="text" class="form-control" name="alias" value="<?= isset($view_items) ? $view_items->alias : ''?>">

<input type="submit" class="btn btn-lg btn-primary" value="Save">
<input type="hidden" name="id" value="<?= isset($view_items) ? $view_items->id : ''?>" >
</form>