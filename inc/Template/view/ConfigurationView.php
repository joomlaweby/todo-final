<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 14/07/2019
 * Time: 16:56
 */

defined('_APP_EXEC') or die;
App\WebApplication::displaySystemMessages();
?>

<form method="post" id="settings-form" class="" action="/config/set">
    <h1 class="h2 mb-3 font-weight-normal">Global settings</h1>
    <div class="container-fluid">

        <ul id="settings-nav" class="nav nav-tabs mb-4" role="tablist">

            <li class="nav-item">
                <a class="nav-link active" href="#site" id="site-tab" role="tab" data-toggle="tab" aria-controls="site" aria-expanded="true">Site</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="#server" role="tab" id="server-tab" data-toggle="tab" aria-controls="server">Server</a>
            </li>

            <!-- Dropdown -->
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" id="system-tab" href="#system" role="tab" aria-controls="system">System</a>
            </li>

        </ul>

        <!-- Content Panel -->
        <div id="clothing-nav-content" class="tab-content">

            <div role="tabpanel" class="tab-pane fade show active" id="site" aria-labelledby="site-tab">
                <h3>Metadata</h3>
                <label for="name" class="col-sm-2 col-form-label"><strong>Site title</strong></label>
                <input type="text" class="form-control" name="name" value="<?= isset($view_formdata) ? $view_formdata->metadata->title : ''?>">
                <label for="meta-description" class="col-sm-2 col-form-label"><strong>Meta Description</strong></label>
                <textarea type="text" class="form-control" name="meta-description" value=""><?= isset($view_formdata) ? $view_formdata->metadata->meta_description : ''?></textarea>
                <label for="meta-keywords" class="col-sm-2 col-form-label"><strong>Meta keywords</strong></label>
                <textarea class="form-control" name="meta-keywords" value=""><?= isset($view_formdata) ? $view_formdata->metadata->meta_keywords : ''?></textarea>
                <label for="base-url" class="col-sm-2 col-form-label"><strong>Website URL</strong></label>
                <input type="text" class="form-control" name="base-url" value="<?= isset($view_formdata) ? $view_formdata->url : ''?>">
                <label for="robots" class="col-sm-2 col-form-label"><strong>Robots</strong></label>
                <select name="robots" class="form-control">
                    <option value="no index, nofollow">No index, no follow</option>
                    <option value="index, follow">Index, Follow</option>
                </select>
                <h3>Image processing</h3>
                <label for="uploads-directory" class="col-sm-2 col-form-label"><strong>Uploads directory</strong></label>
                <input type="text" class="form-control" name="uploads-directory" value="<?= isset($view_formdata) ? $view_formdata->filesystem->uploads_directory : ''?>">
                <label for="allowed-extensions" class="col-sm-2 col-form-label"><strong>Allowed extensions</strong></label>
                <input type="text" class="form-control" name="allowed-extensions" value="<?= isset($view_formdata) ? $view_formdata->filesystem->allowed_extensions : ''?>">
                <label for="maximum-filesize" class="col-sm-2 col-form-label"><strong>Max. filesize</strong></label>
                <input type="text" class="form-control" name="maximum-filesize" value="<?= isset($view_formdata) ? $view_formdata->filesystem->max_size : ''?>">
                <h3>Local settings</h3>
                <label for="date-format" class="col-sm-2 col-form-label"><strong>Date format</strong></label>
                <select name="date-format" class="form-control">
                    <option value="l jS \\of F Y">l jS \\of F Y</option>
                    <option value="D m Y">D m Y</option>
                </select>
            </div>

            <div role="tabpanel" class="tab-pane fade" id="server" aria-labelledby="server-tab">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <h3>Database</h3>
                            <label for="database-host" class="col-sm-6 col-form-label"><strong>Database server</strong></label>
                            <input type="text" class="form-control" name="database-host" value="<?= isset($view_formdata) ? $view_formdata->database->host : ''?>">
                            <label for="database-name" class="col-sm-6 col-form-label"><strong>Database name</strong></label>
                            <input type="text" class="form-control" name="database-name" value="<?= isset($view_formdata) ? $view_formdata->database->database : ''?>">
                            <label for="database-user" class="col-sm-6 col-form-label"><strong>Database user</strong></label>
                            <input type="text" class="form-control" name="database-user" value="<?= isset($view_formdata) ? $view_formdata->database->user : ''?>">
                            <label for="database-password" class="col-sm-6 col-form-label"><strong>Database password</strong></label>
                            <input type="password" class="form-control" name="url" value="" placeholder="" autocomplete="on">
                        </div>
                        <div class="col-sm-6">
                            <h3>E-mail</h3>
                        </div>
                    </div>
                </div>


            </div>

            <div role="tabpanel" class="tab-pane fade" id="system" aria-labelledby="system-tab">
                <h3>Debugging</h3>
                <label for="debug" class="col-sm-2 col-form-label"><strong>Debug</strong></label>
                <select name="debug" class="form-control">
                    <option value="true">Yes</option>
                    <option value="false">No</option>
                </select>
                <label for="template-debug" class="col-sm-2 col-form-label"><strong>Template debug</strong></label>
                <select name="template-debug" class="form-control">
                    <option value="true">Yes</option>
                    <option value="false">No</option>
                </select>
            </div>


        </div>


    </div>







    <input type="submit" class="btn btn-lg btn-primary" value="Save">
</form>
