<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 27/07/2019
 * Time: 11:35
 */


defined('_APP_EXEC') or die;

require_once _APP_LOC . '/inc/Admin/Components/pages/helpers/AdminLayoutRender.php';

App\WebApplication::displaySystemMessages();

?>

<h2>Edit layout of page: <?php echo $view_items->name ?></h2>
<?php

AdminLayoutRender($view_items->layout)

?>
