<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 24/07/2019
 * Time: 23:33
 */
include_once _APP_LOC . '/inc/Template/layouts/toolbar.php';
App\WebApplication::displaySystemMessages();
?>


<table class='table clearfix table-striped item-list'>

    <thead class='thead-dark'>
    <tr>
        <th>#</th>
        <th>Page</th>
        <th>Category</th>
        <th>Published</th>
        <th>Created At</th>
        <th>Actions</th>
    </tr>
    </thead>

    <?php

    $lastRowIndex = count ((array)$view_items) - 1;
    ?>
    <?php foreach ((array)$view_items as $key => $value) : ?>

        <tr><td><?php echo $value->id ?></td>
            <td><h4 class="text-primary"><?php echo $value->name ?></h4></td>
            <td>
                <p class="text-primary"><?php if (isset($value->category)) {echo $value->category;} ?></p>
            </td>

            <td>
                <?php if ($value->published == true) : ?>

                    <a href='/page/unpublishItem?id=<?php echo $value->id ?>' class='glyphicon glyphicon-ok text-success' title='is active'></a>

                <?php endif ?>

                <?php if ($value->published == false) : ?>

                    <a href='/page/publishItem?id=<?php echo $value->id ?>' class='glyphicon glyphicon-remove text-danger' title='is active'></a>

                <?php endif ?>
            </td>

            <td>
                <?php echo substr($value->createdAt, 0, 10) ?>
            </td>
            <td class='actions'>
                <a href='/page/editLayout?id=<?php echo $value->id ?>' class='glyphicon glyphicon-th-list' title='Edit layout'></a>
                <a href='/page/deleteItem?id=<?php echo $value->id ?>' class='glyphicon glyphicon-trash delete-link' title='Delete page'></a>
                <a href='/page/getItem?id=<?php echo $value->id ?>' class='glyphicon glyphicon-edit edit-link' title='Edit page'></a>

                <?php if($key != 0) : ?>
                    <a href='/page/moveUp?id=<?php echo $value->id ?>' class='glyphicon glyphicon-arrow-up' title='move up'></a>
                <?php endif ?>

                <?php if($lastRowIndex != $key) : ?>
                    <a href='/page/moveDown?id=<?php echo $value->id ?>' class='glyphicon glyphicon-arrow-down' title='move down'></a>
                <?php endif ?>
            </td>
        </tr>
    <?php endforeach ?>
</table>