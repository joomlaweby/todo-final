<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 17/07/2019
 * Time: 18:48
 */

defined('_APP_EXEC') or die;
App\WebApplication::displaySystemMessages();
if ($view_formdata) {
    $action = "updateItem";
    $isEdit = true;
} else {
    $action = "insertItem";
    $isEdit = false;
}
?>
<div class="row">
    <form method="post" class="col-sm-4" action="/Todolist/<?= $action ?>" enctype="multipart/form-data">
        <div class="form-group">
            <input type="hidden" name="id" value="<?= $isEdit ? $_GET['id'] : 'false' ?>">
            <input type="hidden" name="file" value="<?= isset($view_formdata) ? $view_formdata->file : '' ?>">
            <label class="col-sm-2 col-form-label"><strong>Deadline</strong></label>
            <input type="date" class="form-control" name="date"
                   value="<?= isset($view_formdata) ? substr($view_formdata->date, 0, 10) : '' ?>">
            <label for="task" class="col-sm-2 col-form-label"><strong>Title</strong></label>
            <input type="text" class="form-control" name="task"
                   value="<?= isset($view_formdata) ? $view_formdata->name : '' ?>">
            <label for="fileUpload" class="col-sm-2 col-form-label"><strong>Image</strong></label>

            <div class="input-group mb-5">
                <div class="input-group-prepend">
                    <span class="input-group-text">Upload</span>
                </div>
                <div class="custom-file">
                    <input type="file" class="custom-file-input" name="fileUpload" id="fileUpload">
                    <label class="custom-file-label" for="fileUpload">Choose file</label>
                </div>
            </div>
            <input type="submit" class="btn btn-primary btn-block"><br>
        </div>

    </form>
</div>
