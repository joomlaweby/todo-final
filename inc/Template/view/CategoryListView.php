<?php defined('_APP_EXEC') or die;

include_once _APP_LOC . '/inc/Template/layouts/toolbar.php';
App\WebApplication::displaySystemMessages();
?>

<table class='table clearfix table-striped item-list'>

    <thead class='thead-dark'>
    <tr>
        <th>#</th>
        <th>Category</th>
        <th>Alias</th>
        <th>Published</th>
        <th>Created At</th>
        <th>Actions</th>
    </tr>
    </thead>

    <?php

    $lastRowIndex = count ((array)$view_items) - 1;
    ?>
    <?php foreach ((array)$view_items as $key => $value) : ?>

        <tr><td><?php echo $value->id ?></td>
            <td><h4 class="text-primary"><?php echo $value->name ?></h4></td>
            <td>
                <p class="text-primary"><?php echo $value->alias ?></p>
            </td>

            <td>
                <?php if ($value->published == true) : ?>

                    <a href='/category/unpublishItem?id=<?php echo $value->id ?>' class='glyphicon glyphicon-ok text-success' title='is active'></a>

                <?php endif ?>

                <?php if ($value->published == false) : ?>

                    <a href='/category/publishItem?id=<?php echo $value->id ?>' class='glyphicon glyphicon-remove text-danger' title='is active'></a>

                <?php endif ?>
            </td>

            <td>
                <?php echo substr($value->createdAt, 0, 10) ?>
            </td>
            <td class='actions'>
                <a href='/category/deleteItem?id=<?php echo $value->id ?>' class='glyphicon glyphicon-trash delete-link' title='Delete category'></a>
                <a href='/category/getItem?id=<?php echo $value->id ?>' class='glyphicon glyphicon-edit edit-link' title='Edit category'></a>

                <?php if($key != 0) : ?>
                    <a href='/category/moveUp?id=<?php echo $value->id ?>' class='glyphicon glyphicon-arrow-up' title='move up'></a>
                <?php endif ?>

                <?php if($lastRowIndex != $key) : ?>
                    <a href='/category/moveDown?id=<?php echo $value->id ?>' class='glyphicon glyphicon-arrow-down' title='move down'></a>
                <?php endif ?>
            </td>
        </tr>
    <?php endforeach ?>
</table>