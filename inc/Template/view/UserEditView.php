<?php

App\WebApplication::displaySystemMessages();

if ($view_items) {
    $action = "updateUser";
    $isEdit = true;
} else {
    $action = "insertUser";
    $isEdit = false;
}
?>
<form method="post" class="form-signin" action="/User/<?php echo $action ?>>
    <h1 class="h3 mb-3 font-weight-normal">New account</h1>
    <label for="loginform-username" class="sr-only">Username</label>
    <input type="text" id="loginform-username" class="form-control" placeholder="<?= isset($view_items) ? $view_items->username : 'Username'?>" name="username" value="<?= isset($view_items) ? $view_items->username : ''?>">
    <label for="password" class="sr-only">Password</label>
    <input type="password" name="password"  placeholder="Password" class="form-control" value="">
    <label for="verify-username" class="sr-only">Verify password</label>
    <input type="verify-password" name="verify-password"  placeholder="Verify password" class="form-control" value="">
    <label for="email" class="sr-only">Email address</label>
    <input type="text" name="email"  placeholder="Email address" class="form-control" value="<?= isset($view_items) ? $view_items->email : ''?>">
    <input type="submit" class="btn btn-lg btn-primary btn-block" value="Register">
</form>
