<?php defined('_APP_EXEC') or die;
App\WebApplication::displaySystemMessages();
if ($view_formdata) {
    $action = "updateItem";
    $isEdit = true;
} else {
    $action = "insertItem";
    $isEdit = false;
}

if (isset(App\WebApplication::$viewData)) {
    $menu = App\WebApplication::$viewData->menu;
}
else {
    $menu = [];
}


?>

    <form method="post" id="menu-item-form" class="" action="/menu/<?php echo $action ?>">
        <h1 class="h2 mb-3 font-weight-normal">Menu item</h1>
        <label for="name" class="col-sm-2 col-form-label"><strong>Name</strong></label>
        <input type="text" class="form-control" name="name" required value="<?= isset($view_formdata) ? $view_formdata->name : ''?>">
        <label for="alias" class="col-sm-2 col-form-label"><strong>Alias</strong></label>
        <input type="text" class="form-control" name="alias" value="<?= isset($view_formdata) ? $view_formdata->alias : ''?>">
        <label for="url" class="col-sm-2 col-form-label"><strong>URL</strong></label>
        <input type="text" class="form-control" name="url" value="<?= isset($view_formdata) ? $view_formdata->url : ''?>">
        <label for="menu" class="col-sm-2 col-form-label"><strong>Menu</strong></label>
        <select class="form-control" name="menu">
            <option value="1">Admin menu</option>
            <option value="2">Main menu</option>
        </select>

        <input type="hidden" class="btn btn-lg btn-primary" name="id" value="<?= isset($view_formdata) ? $view_formdata->id : ''?>">
        <input type="submit" class="btn btn-lg btn-primary" value="Save">
    </form>

<?php
