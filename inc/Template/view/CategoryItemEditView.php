<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 15/07/2019
 * Time: 20:20
 */

defined('_APP_EXEC') or die; ?>

    <form method="post" id="menu-item-form" class="" action="/category/insertitem">
        <h1 class="h2 mb-3 font-weight-normal">Category</h1>
        <label for="name" class="col-sm-2 col-form-label"><strong>Name</strong></label>
        <input type="text" class="form-control" name="name" value="<?= isset($view_formdata) ? $view_formdata->name : ''?>">
        <label for="alias" class="col-sm-2 col-form-label"><strong>Alias</strong></label>
        <input type="text" class="form-control" name="alias" value="<?= isset($view_formdata) ? $view_formdata->alias : ''?>">
        <label for="parent" class="col-sm-2 col-form-label"><strong>Parent</strong></label>
        <input type="text" class="form-control" name="url" value="<?= isset($view_formdata) ? $view_formdata->parent : ''?>">
        <input type="submit" class="btn btn-lg btn-primary" value="Save">
    </form>

