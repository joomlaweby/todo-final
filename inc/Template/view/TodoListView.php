<?php


App\WebApplication::displaySystemMessages();

        if (isset($view_formdata)) {
            $action = "updateItem";
            $isEdit = true;
        } else {
            $action = "insertItem";
            $isEdit = false;
        }

        if (($isEdit) and ((trim($view_formdata->file) != 'assets/') and file_exists($view_formdata->file))) {
            list($width, $height) = getimagesize($view_formdata->file);
        }
//$userId = $this->getApp()->getSession()->get('user.id');

?>
<div class="row">
    <form method="post" class="col-sm-4" action="/Todolist/<?=$action?>" enctype="multipart/form-data">
        <div class="form-group">
            <input type="hidden" name="id" value="<?= $isEdit ? $_GET['id'] : 'false' ?>">
            <input type="hidden" name="file" value="<?= isset($view_formdata) ? $view_formdata->file : ''?>">
            <label class="col-sm-2 col-form-label"><strong>Deadline</strong></label>
            <input type="date" class="form-control" name="date" value="<?= isset($view_formdata) ? substr($view_formdata->date, 0, 10) : ''?>">
            <label for="task" class="col-sm-2 col-form-label"><strong>Title</strong></label>
            <input type="text" class="form-control" name="task" value="<?= isset($view_formdata) ? $view_formdata->name : ''?>">
            <label for="fileUpload" class="col-sm-2 col-form-label"><strong>Image</strong></label>
            <div class="input-group mb-5">
                <div class="input-group-prepend">
                    <span class="input-group-text">Upload</span>
                </div>
                <div class="custom-file">
                    <input type="file" class="custom-file-input" name="fileUpload" id="fileUpload">
                    <label class="custom-file-label" for="fileUpload">Choose file</label>
                </div>
            </div>
            <input type="submit" class="btn btn-primary btn-block"><br>
        </div>

    </form>
    <div class="col-sm-8 pull-right text-left">
        <?php if ($isEdit) : ?>
            <h3 class="mb-5"><span class="text-muted">Editing item:</span> <span class="text-primary float-right text-right"><?php echo $view_formdata->name ?><br></span></h3>
            <?php if ((trim($view_formdata->file) != 'uploads/') and file_exists($view_formdata->file)) : ?>
                <img src="/<?php echo $view_formdata->file ?>" class="clearfix mb-5" width='200' height='auto'/>
            <?php endif ?>
            <ul class="list-group list-group-flush mb-3">
                <li class="list-group-item"><strong class="text-primary">Deadline:</strong> <?php echo substr($view_formdata->date, 0, 10) ?>
                    <?php if (countDaysleft($view_formdata->date) > 1) : ?>
                        <span class="text-muted float-right"><?php echo countDaysleft($view_formdata->date) ?> days left</span>
                    <?php endif ?>
                    <?php if (countDaysleft($view_formdata->date) == 1) : ?>
                        <span class="text-muted float-right"><?php echo countDaysleft($view_formdata->date) ?> day left</span>
                    <?php endif ?>
                    <?php if (countDaysleft($view_formdata->date) < 0) : ?>
                        <span class="text-warning float-right"><?php echo abs(countDaysleft($view_formdata->date)) ?> days overdue</span>
                    <?php endif ?>
                    <?php if (countDaysleft($view_formdata->date) == 0) : ?>
                        <span class="text-warning float-right"><strong>Today is your last chance to do it!</strong></span>
                    <?php endif ?>

                </li>
                <li class="list-group-item">
                    <?php if ((trim($view_formdata->file) != 'assets/') and file_exists($view_formdata->file)) : ?>
                    <strong class="text-primary">Image: </strong><?php echo str_replace("uploads/","",$view_formdata->file) ?><span class="delete-button"><a href="/Todolist/deleteFile?id=<?php echo $_GET['id'] ?>" class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>&nbsp;Delete file</a></span><span class="text-muted float-right"><?php echo $width ?>px x <?php echo $height ?>px</span></li>
                <?php endif ?>
            </ul>
        <?php endif; ?>
    </div>
    <div class="clearfix"></div>
</div>


    <table class='table clearfix item-list'>

    <thead class='thead-dark'>
    <tr>
        <th>#</th>
        <th>Task</th>
        <th>Deadline</th>
        <th>File</th>
        <th>Actions</th>
    </tr>
    </thead>

    <?php

    $lastRowIndex = count ((array)$view_items) - 1;
    ?>
    <?php foreach ((array)$view_items as $key => $value) : ?>

        <tr><td><?php echo $value->id ?></td>
            <td><h4 class="text-primary"><?php echo $value->name ?></h4></td>
            <td><?php echo $value->date ?><br>
                <?php if (countDaysleft($value->date) > 1) : ?>
                    <span class="text-muted"><?php echo countDaysleft($value->date) ?> days left</span>
                <?php endif ?>
                <?php if (countDaysleft($value->date) == 1) : ?>
                    <span class="text-muted"><?php echo countDaysleft($value->date) ?> day left</span>
                <?php endif ?>
                <?php if (countDaysleft($value->date) < 0) : ?>
                    <span class="text-warning"><strong><?php echo abs(countDaysleft($value->date)) ?> days after deadline</strong></span>
                <?php endif ?>
                <?php if (countDaysleft($value->date) == 0) : ?>
                    <span class="text-warning"><strong>Today is your last chance to do it!</strong></span>
                <?php endif ?>
            </td>
            <td>
                <?php if ((trim($value->file) != 'uploads/' . $userId) and file_exists($value->file)) : ?>
                    <a href="/<?php echo $value->file ?>" download><img src="/<?php echo $value->file ?>" width='160' height='auto'></a>
                <?php endif ?>
            </td>
            <td class='actions'>
                <a href='/Todolist/deleteItem?id=<?php echo $value->id ?>' class='glyphicon glyphicon-trash delete-link' title='delete'></a>
                <a href='/Todolist/getItem?id=<?php echo $value->id ?>' class='glyphicon glyphicon-edit edit-link' title='edit'></a>

                <?php if($key != 0) : ?>
                    <a href='/Todolist/moveUp?id=<?php echo $value->id ?>' class='glyphicon glyphicon-arrow-up' title='move up'></a>
                <?php endif ?>

                <?php if($lastRowIndex != $key) : ?>
                    <a href='/Todolist/moveDown?id=<?php echo $value->id ?>' class='glyphicon glyphicon-arrow-down' title='move down'></a>
                <?php endif ?>

            </td>
        </tr>
    <?php endforeach ?>
</table>

<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 30/06/2019
 * Time: 23:02
 */