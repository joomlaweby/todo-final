<?php

defined('_APP_EXEC') or die;
include_once _APP_LOC . '/inc/Template/layouts/toolbar.php';
App\WebApplication::displaySystemMessages();
?>


<table class='table clearfix table-striped item-list'>

        <thead class='thead-dark'>
        <tr>
            <th>#</th>
            <th>Username</th>
            <th>Is Active</th>
            <th>Is Admin</th>
            <th>Created At</th>
            <th>Actions</th>
        </tr>
        </thead>

        <?php
        $lastRowIndex = count ($view_items) - 1;


        ?>
<?php foreach ($view_items as $key => $value) : ?>

    <tr><td><?php echo $value->id ?></td>
        <td><h4 class="text-primary"><?php echo $value->username ?></h4></td>
        <td>
            <?php if ($value->published == true) : ?>

                <a href='/user/unpublishItem?id=<?php echo $value->id ?>' class='glyphicon glyphicon-ok text-success' title='is active'></a>

            <?php endif ?>

            <?php if ($value->published == false) : ?>

                <a href='/user/publishItem?id=<?php echo $value->id ?>' class='glyphicon glyphicon-remove text-danger' title='is active'></a>

            <?php endif ?>

        </td>

        <td>


            <?php if ($value->isAdmin == true) : ?>

                <a href='/user/getItem?id=<?php echo $value->id ?>' class='glyphicon glyphicon-ok text-success' title='is admin'></a>

            <?php endif ?>

            <?php if ($value->isAdmin == false) : ?>

                <a href='/user/getItem?id=<?php echo $value->id ?>' class='glyphicon glyphicon-remove text-danger' title='is admin'></a>

            <?php endif ?>

        </td>
        <td>
            <?php echo substr($value->createdAt, 0, 10) ?>
        </td>
        <td class='actions'>
            <a href='/user/deleteUser?id=<?php echo $value->id ?>' class='glyphicon glyphicon-trash text-danger' title='delete user'></a>
            <a href='/user/getItem?id=<?php echo $value->id ?>' class='glyphicon glyphicon-edit text-primary' title='edit user'></a>

            <?php if($key != 0) : ?>
                <a href='/user/moveUp?id=<?php echo $value->id ?>' class='glyphicon glyphicon-arrow-up' title='move up'></a>
            <?php endif ?>

            <?php if($lastRowIndex != $key) : ?>
                <a href='/user/moveDown?id=<?php echo $value->id ?>' class='glyphicon glyphicon-arrow-down' title='move down'></a>
            <?php endif ?>

        </td>
    </tr>
<?php endforeach ?>
</table>