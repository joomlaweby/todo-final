<?php App\WebApplication::displaySystemMessages(); ?>
<form method="post" class="form-signin" action="/User/loginSubmit">
    <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
    <label for="loginform-username" class="sr-only">Username</label>
    <input type="text" id="loginform-username" class="form-control" placeholder="Username" name="username" value="">
    <label for="username" class="sr-only">Password</label>
    <input type="password" name="password"  placeholder="Password" class="form-control" value="">
    <div class="checkbox mb-3">
        <label>
            <input type="checkbox" name="remember-me"> Remember me</input>
        </label>
    </div>
    <input type="submit" class="btn btn-lg btn-primary btn-block">
</form>
