<?php App\WebApplication::displaySystemMessages(); ?>
<form method="post" class="form-signin" action="/User/registerSubmit">
    <h1 class="h3 mb-3 font-weight-normal">New account</h1>
    <label for="loginform-username" class="sr-only">Username</label>
    <input type="text" id="loginform-username" class="form-control" placeholder="Username" name="username" value="">
    <label for="username" class="sr-only">Password</label>
    <input type="password" name="password"  placeholder="Password" class="form-control" value="">
    <label for="username" class="sr-only">Verify password</label>
    <input type="password" name="verify-password"  placeholder="Verify password" class="form-control" value="">
    <label for="email" class="sr-only">Email address</label>
    <input type="text" name="email"  placeholder="Email address" class="form-control" value="">
    <input type="submit" class="btn btn-lg btn-primary btn-block" value="Register">
</form>
