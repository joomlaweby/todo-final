<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 27/07/2019
 * Time: 15:08
 */

defined('_APP_EXEC') or die;

App\WebApplication::displaySystemMessages();
$action = "updateItem";
if ($view_items) {

    $isEdit = true;
} else {
    $isEdit = false;
}

?>

<form method="post" id="page-element-form" class="" action="/page/<?= $action ?>">
    <h1 class="h2 mb-3 font-weight-normal">Page element</h1>
    <label for="name" class="col-sm-2 col-form-label"><strong>Name</strong></label>
    <input type="text" class="form-control" name="name" value="<?= isset($view_items) ? $view_items->name : ''?>">

    <input type="submit" class="btn btn-lg btn-primary" value="Save">
    <input type="hidden" name="id" value="<?= isset($view_items) ? $view_items->id : ''?>" >
</form>