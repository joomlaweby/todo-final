<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 24/07/2019
 * Time: 23:33
 */

defined('_APP_EXEC') or die;

App\WebApplication::displaySystemMessages();

if ($view_items) {
    $action = "updateItem";
    $isEdit = true;
} else {
    $action = "insertItem";
    $isEdit = false;
}

if (isset(App\WebApplication::$viewData)) {
    $categories = App\WebApplication::$viewData->categories;
    $tags = App\WebApplication::$viewData->tags;
}
else {
    $categories = [];
    $tags = [];
}

?>

<form method="post" id="page-item-form" class="" action="/page/<?= $action ?>">
    <h1 class="h2 mb-3 font-weight-normal">Page</h1>
    <label for="name" class="col-sm-2 col-form-label"><strong>Name</strong></label>
    <input type="text" class="form-control" name="name" value="<?= isset($view_items) ? $view_items->name : ''?>">
    <label for="alias" class="col-sm-2 col-form-label"><strong>Alias</strong></label>
    <input type="text" class="form-control" name="alias" value="<?= isset($view_items) ? $view_items->alias : ''?>">
    <div class="form-group">
        <label for="category" class="col-sm-2 col-form-label">Category</label>
        <select class="form-control" id="category" name="category">
            <?php foreach ($categories as $category) : ?>
                <option value="<?= $category->id ?>" <?= $view_items->category == $category->id ?  'selected' : ''?>><?= $category->name ?></option>
            <?php endforeach ?>
        </select>
    </div>
    <h3 class="border-bottom">Metadata</h3>
    <label for="title" class="col-sm-2 col-form-label"><strong>Title (in browser)</strong></label>
    <input type="text" class="form-control" name="title" value="<?= isset($view_items) ? $view_items->title : ''?>">
    <input type="submit" class="btn btn-lg btn-primary" value="Save">
    <input type="hidden" name="id" value="<?= isset($view_items) ? $view_items->id : ''?>" >
</form>