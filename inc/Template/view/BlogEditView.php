<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 15/07/2019
 * Time: 20:20
 */

defined('_APP_EXEC') or die;
App\WebApplication::displaySystemMessages();
if ($view_items) {
    $action = "updateItem";
    $isEdit = true;
} else {
    $action = "insertItem";
    $isEdit = false;
}

if (isset(App\WebApplication::$viewData)) {
    $categories = App\WebApplication::$viewData->categories;
    $tags = App\WebApplication::$viewData->tags;
}
else {
    $categories = [];
    $tags = [];
}
?>

    <form method="post" id="menu-item-form" class="" action="/blog/<?= $action ?>" enctype="multipart/form-data">
        <h1 class="h2 mb-3 font-weight-normal">Article</h1>
        <div class="form-group">
            <label for="name" class="col-sm-2 col-form-label">Name</label>
            <input type="text" class="form-control" name="name" value="<?= isset($view_items) ? $view_items->name : ''?>">
        </div>
        <div class="form-group">
            <label for="alias" class="col-sm-2 col-form-label">Alias</label>
            <input type="text" class="form-control" name="alias" value="<?= isset($view_items) ? $view_items->alias : ''?>">
        </div>

        <div class="form-group">
            <label for="category" class="col-sm-2 col-form-label">Category</label>
            <select class="form-control" id="category" name="category">
                <?php foreach ($categories as $category) : ?>
                    <option value="<?= $category->id ?>" <?= $view_items->category == $category->id ?  'selected' : ''?>><?= $category->name ?></option>
                <?php endforeach ?>
            </select>
        </div>
        <div class="form-group">
            <label for="tags" class="col-sm-2 col-form-label">Tags</label>
            <select class="form-control chosen-select" data-placeholder="Choose tags..." id="tags" name="tags[]" multiple tabindex="-1">
                <?php foreach ($tags as $tag) : ?>
                    <option value="<?= $tag->id ?>" <?= $view_items->category == $tag->id ?  'selected' : ''?>><?= $tag->name ?></option>
                <?php endforeach ?>
            </select>
        </div>
        <div class="form-group">
            <label for="image" class="col-sm-2 col-form-label">Article image</label>
            <div class="input-group mb-5">

                <div class="input-group-prepend">
                    <span class="input-group-text">Upload</span>
                </div>
                <div class="custom-file">
                    <input type="file" class="custom-file-input" name="image" id="edit-form-image">
                    <label class="custom-file-label" for="image">Choose file</label>
                </div>
            </div>
        </div>


        <div class="form-group">
            <label for="text" class="col-sm-2 col-form-label">Text</label>
            <div class="form-control" id="editor" name="text">

                <textarea name="text" id="text">
<?= isset($view_items) ? $view_items->text : ''?>
                </textarea>

            </div>
        </div>
        <input type="submit" class="btn btn-lg btn-primary" value="Save">
        <input type="hidden" name="id" value="<?= isset($view_items) ? $view_items->id : ''?>" >
    </form>
<!-- Initialize Quill editor -->
<script>
    var quill = new Quill('#editor', {
        theme: 'snow'
    });
</script>
