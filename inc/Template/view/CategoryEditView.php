<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 15/07/2019
 * Time: 20:20
 */

defined('_APP_EXEC') or die;
App\WebApplication::displaySystemMessages();
if ($view_items) {
    $action = "updateItem";
    $isEdit = true;
} else {
    $action = "insertItem";
    $isEdit = false;
}

if (isset(App\WebApplication::$viewData)) {
    $categories = App\WebApplication::$viewData->categories;
}
else {
    $categories = '';
}
?>

    <form method="post" id="menu-item-form" class="" action="/category/<?= $action ?>">
        <h1 class="h2 mb-3 font-weight-normal">Category</h1>
        <label for="name" class="col-sm-2 col-form-label"><strong>Name</strong></label>
        <input type="text" class="form-control" name="name" value="<?= isset($view_items) ? $view_items->name : ''?>">
        <label for="alias" class="col-sm-2 col-form-label"><strong>Alias</strong></label>
        <input type="text" class="form-control" name="alias" value="<?= isset($view_items) ? $view_items->alias : ''?>">
        <div class="form-group">
            <label for="parent-category">Parent category</label>
            <select class="form-control" id="parent-category" name="parent-category">
                <?php foreach ($categories as $category) : ?>
                <option value="<?= $category->id ?>" <?= $view_items->parent == $category->id ?  'selected' : ''?>><?= $category->name ?></option>
                <?php endforeach ?>
            </select>
        </div>
        <input type="submit" class="btn btn-lg btn-primary" value="Save">
        <input type="hidden" name="id" value="<?= isset($view_items) ? $view_items->id : ''?>" >
    </form>

