<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 30/06/2019
 * Time: 19:35
 */
defined('_APP_EXEC') or die;


?>

<nav class="navbar navbar-expand-lg">
    <a class="navbar-brand  text-primary" href="/">ToDo App</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/">Tasks <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/user/settings">
                    My Settings
                </a>

            </li>
            <li class="nav-item">
                <a class="nav-link" href="/blog/getall">
                    Blog
                </a>

            </li>
            <?php if ($this->getApp()->isAdmin()) : ?>
            <li class="nav-item">
                <a class="nav-link" href="/admin/index.php">
                    Administrator
                </a>

            </li>
            <?php endif ?>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <p class="pull-right">Hello, <?php echo $_SESSION['user']['username'] . ' '; ?><a href="/user/logoutSubmit" class="btn btn-warning">Logout</a></p>
        </form>
    </div>
</nav>

<?php
