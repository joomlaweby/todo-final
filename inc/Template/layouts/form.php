<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 30/04/2019
 * Time: 22:01
 */
defined('_APP_EXEC') or die;

if ($app->isValidGetOperation('edit') and file_exists($form_data->file)) {
    list($width, $height) = getimagesize($form_data->file);
}

?>
    <div class="row">
        <form method="post" id="edit-form" class="col-sm-4" action="index.php" enctype="multipart/form-data">
            <div class="form-group">
                <label class="col-sm-2 col-form-label"><strong>Deadline</strong></label>
                <input type="date" class="form-control" name="date" value="<?= isset($form_data) ? $form_data->deadline : ''?>">
                <label for="task" class="col-sm-2 col-form-label"><strong>Title</strong></label>
                <input type="text" class="form-control" id="task" placeholder="New item" name="task" value="<?= isset($form_data) ? $form_data->name : ''?>">
                <label class="col-sm-2 col-form-label"><strong>Status</strong></label>
                <select class="form-control" name="status" value="<?= isset($form_data) ? $form_data->status : ''?>">
                    <option value="0">New</option>
                    <option value="1">In progress</option>
                    <option value="2">Closed</option>
                </select>
                <label for="fileToUpload" class="col-sm-2 col-form-label"><strong>Image</strong></label>
                <div class="input-group mb-5">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Upload</span>
                    </div>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" name="fileToUpload"  id="fileToUpload">
                        <label class="custom-file-label" for="fileToUpload">Choose file</label>
                    </div>
                </div>

                <!-- <textarea name="task"></textarea> -->
                <input type="submit" name="submit" class="btn btn-primary btn-block"><br>
            </div>
            <input type="hidden" name="id" value="<?= $app->isValidGetOperation('edit') ? $_GET['id'] : 'false' ?>">
            <input type="hidden" name="file" value="<?= isset($form_data) ? $form_data->file : ''?>">
        </form>
        <div class="col-sm-8 pull-right text-left">
            <?php if ($app->isValidGetOperation('edit')) : ?>
                <h3 class="mb-5"><span class="text-muted">Editing item:</span> <span class="text-primary float-right text-right"><?php echo $form_data->name ?><br></span></h3>

                <img src="<?php echo $form_data->file ?>" class="clearfix mb-5" width='200' height='auto'/>
                <ul class="list-group list-group-flush mb-3">
                    <li class="list-group-item"><strong class="text-primary">Deadline:</strong> <?php echo $form_data->deadline ?>
                        <?php if ($todo_list->countDaysleft($form_data->deadline) > 1) : ?>
                            <span class="text-muted float-right"><?php echo $todo_list->countDaysleft($form_data->deadline) ?> days left</span>
                        <?php endif ?>
                        <?php if ($todo_list->countDaysleft($form_data->deadline) == 1) : ?>
                            <span class="text-muted float-right"><?php echo $todo_list->countDaysleft($form_data->deadline) ?> day left</span>
                        <?php endif ?>
                        <?php if ($todo_list->countDaysleft($form_data->deadline) < 0) : ?>
                            <span class="text-warning float-right"><?php echo abs($todo_list->countDaysleft($form_data->deadline)) ?> days overdue</span>
                        <?php endif ?>
                        <?php if ($todo_list->countDaysleft($form_data->deadline) == 0) : ?>
                            <span class="text-warning float-right"><strong>Today is your last chance to do it!</strong></span>
                        <?php endif ?>

                    </li>
                    <li class="list-group-item">
                        <?php if (file_exists($form_data->file)) : ?>
                            <strong class="text-primary">Image: </strong><?php echo str_replace("uploads/","",$form_data->file) ?><span class="delete-button"><a href="index.php?operation=deletefile&id=<?= $app->isValidGetOperation('edit') ? $_GET['id'] : '' ?>" class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>&nbsp;Delete file</a></span><span class="text-muted float-right"><?php echo $width ?>px x <?php echo $height ?>px</span></li>
                        <?php endif ?>
                </ul>
            <?php endif; ?>
        </div>
        <div class="clearfix"></div>
    </div>
<?php
