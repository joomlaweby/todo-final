<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 30/04/2019
 * Time: 23:21
 */
defined('_APP_EXEC') or die;
$lastRowIndex = count ((array)$todo_list->tasks) - 1;



/* var_dump($todo_list->tasks); */
?>
<table class='table clearfix' class="item-list">
    <thead class='thead-dark'>
        <tr>
            <th>#</th>
            <th>Task</th>
            <th>Deadline</th>
            <th>File</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>

    <?php foreach ($todo_list->tasks as $key => $value) :  ?>
        <?php
        $status = $value->status;
        if ($status == 0) {
            $status = 'New';
        }
        else if ($status == 1) {
            $status = 'In progress';
        }
        else if ($status == 2) {
            $status = 'Closed';
        }
        ?>

    <tr id="item-<?php echo $value->id ?>">
        <td>
             <span class="text-muted"><?php echo $key ?></span>
        </td>
        <td>
            <h4 class="text-primary"><?php echo $value->name ?></h4>
            <span class="badge badge-secondary"><?php echo $status ?></span>
        </td>
        <td>
            <strong><?php echo $value->deadline; ?></strong><br>
            <?php if ($todo_list->countDaysleft($value->deadline) > 1) : ?>
                <span class="text-muted"><?php echo $todo_list->countDaysleft($value->deadline) ?> days left</span>
            <?php endif ?>
            <?php if ($todo_list->countDaysleft($value->deadline) == 1) : ?>
                <span class="text-muted"><?php echo $todo_list->countDaysleft($value->deadline) ?> day left</span>
            <?php endif ?>
            <?php if ($todo_list->countDaysleft($value->deadline) < 0) : ?>
                <span class="text-warning"><strong><?php echo abs($todo_list->countDaysleft($value->deadline)) ?> days after deadline</strong></span>
            <?php endif ?>
            <?php if ($todo_list->countDaysleft($value->deadline) == 0) : ?>
                <span class="text-warning"><strong>Today is your last chance to do it!</strong></span>
            <?php endif ?>
        </td>
        <td>
            <a href='<?php echo $value->file ?>' download >
                <img width='120' height='auto' src='<?php echo $value->file ?>'>
            </a>
        </td>
        <td class='actions'>
            <a href='index.php?operation=delete&id=<?php echo $value->id ?>' class='glyphicon glyphicon-trash delete-link' title='delete'></a>
            <a class='glyphicon glyphicon-edit edit-link' href='index.php?operation=edit&id=<?php echo $value->id ?>' title='edit'></a>

            <?php if($key != 0) : ?>
                <a href='index.php?operation=moveup&id=<?php echo $value->id ?>'  class='glyphicon glyphicon-arrow-up' title='move up'></a>
            <?php endif ?>

            <?php if($lastRowIndex != $key) : ?>
                <a href='index.php?operation=movedown&id=<?php echo $value->id ?>' class='glyphicon glyphicon-arrow-down' title='move down'></a>
            <?php endif ?>
        </td>
    </tr>
    <?php
        endforeach;
    ?>
    </tbody>
</table>