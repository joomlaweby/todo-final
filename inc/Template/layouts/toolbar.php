<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 11/07/2019
 * Time: 22:45
 */
defined('_APP_EXEC') or die;
?>

<div class="mb-3">
    <a href="/<?php echo $admin_view; ?>/insert" class="btn btn-small button-new btn-success">
        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
        New
    </a>
</div>
