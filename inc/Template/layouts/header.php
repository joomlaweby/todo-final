<?php defined('_APP_EXEC') or die;


$params = json_decode($user_settings->params);
$config = $this->getConfig();

?>


<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="<?php echo $config->get('metadata')->meta_description ?>">
  <meta name="keywords" content="<?php echo $config->get('keywords')->meta_description ?>">
<?php if (isset($params)) : ?>
  <link href="/inc/Template/css/theme-<?php echo $params->style ?>.css" rel="stylesheet" id="theme" type="text/css">
    <?php endif ?>
    <?php if (!isset($params)) : ?>
        <link href="/inc/Template/css/theme-3.css" rel="stylesheet" id="theme" type="text/css">
    <?php endif ?>
  <link rel="stylesheet" href="/inc/Template/css/glyphicons.css" type="text/css">
  <link rel="stylesheet" href="/inc/Template/css/main.css" type="text/css">

  <title><?php echo $config->get('metadata')->title ?></title>

</head>