<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 09/05/2019
 * Time: 20:18
 */
defined('_APP_EXEC') or die;
?>
<div role='alert' class='alert alert-<?= $class  ?>' alert-dismissible fade show>
    <?= $message  ?>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>