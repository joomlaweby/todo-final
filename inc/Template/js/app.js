/**
 * Created by Miro on 31/05/2019.
 */

(function($)
{

    /* input form */
    var form = $("#edit-form");
    var input = form.find("#task");
    var list = $('#item-list tbody');

    /* settings */

    var animation = {
        startColor: '#b02669',
        endColor: list.find('tr').css('backgroundColor') || 'transparent',
        delay: 200
    }


    /*input.val('').focus(); */
    form.on('submit', function(event){

        event.preventDefault();

        var req = $.ajax({
            url: form.attr('action'),
            type: 'POST',
            data: form.serialize(),
            dataType: 'json'
        });
        console.log(req);
        req.done(function(data){
            console.log('test');
            console.log(data.id);
            if (data.status === 'success') {
                $.ajax({url: baseUrl}).done(function(html){
                    var newItem = $(html).find('#item-' + data.id);
                    newItem.appendTo(list)
                        .css({'background-color': animation.startColor})
                        .delay(animation.delay)
                        .animate({'background-color' : animation.endColor});
                    $('#no-items-message').fadeOut('slow');
                });
            };


        });
    });

    input.on('keypress', function(event){
        if (event.which === 13) {

            form.submit();
            form.find('textarea').val('');
            return false;
        }
    });

    /* edit form */

    $('#edit-form').find('#task').select();

    /* delete form */

    $('#delete-form').on('submit', function(event){
        return confirm ('For sure?');
    });

    /* Styles changing */

    $("#settings-form select").change(function() {
        var selected = $("#settings-form select").val();
        $("html").find('link#theme').remove();
        $('head').append('<link rel="stylesheet" href="/inc/Template/css/theme-' + selected + '.css" type="text/css" />');
    });

}(jQuery));
