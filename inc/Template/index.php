<?php
defined('_APP_EXEC') or die;

$db = $this->getApp()->getDb();
$model = new App\Cms\Components\User\Model\UserModel($db);
$userId = $this->getApp()->getSession()->get('user.id');
if ($userId > 0) {
    $user_settings = $model->getSettings();

}
use App\WebApplication;


include_once "layouts/header.php";
?>

    <body>
<?php  ?>
    <main>
        <div class="container">
            <header class="border-bottom border-dark">

                <?php
                if ($userId) {
                     include_once 'layouts/main_menu.php';
                   };
                 ?>

            </header>

            <?php

            WebApplication::displaySystemMessages();

            WebApplication::getView($view, $view_items, $view_formdata);

            ?>




        </div>

    </main>


<?php
include_once _APP_LOC . "/inc/Template/layouts/footer.php";
