<?php
namespace App\Cms\Components\Chuck\Controller;
defined('_APP_EXEC') or die;

class ChuckController {

    function getJoke() {

        $joke = file_get_contents('http://api.icndb.com/jokes/random');
        $joke = json_decode($joke);

        return $joke->value->joke; 
    }

    function execute()
    {
        return $this->getJoke();
    }

}