<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 18/07/2019
 * Time: 20:57
 */
namespace App\Cms\Components\Menu\Model;
defined('_APP_EXEC') or die;
use Joomla\Database;
use Joomla\Database\DatabaseDriver;
use App\Base\Model\AbstractItemModel;
class MenuModel extends AbstractItemModel {
    function __construct(DatabaseDriver $db = null)
    {
        $this->table = $this->getDbTableName();
        parent::__construct($db);
    }
}