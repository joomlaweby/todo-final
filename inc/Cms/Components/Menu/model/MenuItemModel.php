<?php
namespace App\Cms\Components\Menu\Model;
defined('_APP_EXEC') or die;

use Joomla\Database;
use Joomla\Database\DatabaseDriver;
use App\Base\Model;
use App;
use App\Base\Model\AbstractItemModel;
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 07/07/2019
 * Time: 21:04
 */

class MenuItemModel extends AbstractItemModel {

    public $alias;
    public $url;
    private $table;
    /**
     * Metadata description
     *
     * @var    string
     * @since  1.0
     */
    public $metadesc = null;

    /**
     * Key words for metadata
     *
     * @var    string
     * @since  1.0
     */
    public $metakey = null;

    /**
     * JSON string of other metadata
     *
     * @var    string
     * @since  1.0
     */
    public $metadata = null;

    function __construct($db = null)
    {
        $this->table = $this->getDbTableName();
        parent::__construct($db);
    }

    function insertItem() {

        $table = $this->getDbTableName();

        $db = $this->db;

        $sql = "SELECT MAX(ordering) as max_ordering FROM $table";

        $ordering = $db->setQuery($sql)->loadResult();
        $this->id = 0;
        $this->ordering = $ordering + 1;

        $sql = "INSERT INTO $table (id, name, alias, url, ordering, created_by, created_at, updated_at, published)
            VALUES ($this->id, '$this->name', '$this->alias', '$this->url', $this->ordering, $this->createdBy, NOW(), NOW(), '1')";

        return $db->setQuery($sql)->execute();
    }



    function getItem($id) {

        $table = $this->getDbTableName();

        $db = $this->db;

        $iterator = $db->setQuery(
            $db->getQuery(true)->select('*')->from($table)->order('ordering')->setLimit(1)
        )->getIterator();

        // Modify model name to full name
        $model = get_class($this);


        $r = [];
        if (count($iterator) > 0) {
            foreach ($iterator as $row)
            {
                $row = (array)$row;
                $item = new $model();
                $item->id = $row['id'];
                $item->name = $row['name'];
                $item->ordering = $row['ordering'];
                $item->published = $row['published'];
                $item->createdAt = $row['created_at'];
                $item->updatedAt = $row['updated_at'];
                $item->alias = $row['alias'];
                $item->url = $row['url'];
                $r = $item;
            }
        }
        /* Dynamically added property - todo create foreach loop */
        $name = 'test';

        $this->$name = 'Value';


        return $r;




    }

    function updateItem () {
        $table = $this->getDbTableName();

        $db = $this->db;



        $sql = "UPDATE $table
                SET name = '$this->name',
                    url = '$this->url',
                    alias = '$this->alias',
                    updated_at = NOW()
                WHERE id = $this->id";


        return $db->setQuery($sql)->execute();
    }


}