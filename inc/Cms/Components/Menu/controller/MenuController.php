<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 07/07/2019
 * Time: 22:17
 */
namespace App\Cms\Components\Menu\Controller;
defined('_APP_EXEC') or die;
use Joomla\Application\AbstractApplication;
use Joomla\Controller\AbstractController;
use Joomla\Input\Input;
use App\Base\Controller;
use App\Base\Controller\AbstractAppController;
use App\Base\Model;
use App\Cms\Components\Menu\Model\MenuItemModel;
use App;
use Joomla\DI\Container;

class menuController extends AbstractAppController {

    /**
     * The template renderer.
     *
     * @var  RendererInterface
     */
    private $renderer;
    /**
     * The view object.
     *
     * @var  MenuHtmlView
     */
    private $view;

    function __construct($input, $app)
    {
        parent::__construct($input, $app);
        $this->client = 'admin';
    }




    function insertItem() {
        $db = $this->getApp()->getDb();
        $this->isLoggedIn();

        if (!$this->getApp()->isAdmin()) {
            App\WebApplication::alertMessage('You do not have permissions for this action.', 'warning');
            header('Location: /');
            exit;
        }
        $userId = $this->getApp()->getSession()->get('user.id');
        $model = new MenuItemModel($db);
        $model->name = $_POST['name'];
        $model->url = $_POST['url'];
        $model->alias = createAlias($_POST['name']);
        $model->createdBy = $userId;

        if($model->insertItem('menu_item')) {
            App\WebApplication::alertMessage('Item was successfully added.', 'success');
        };

        header('Location: /menu/getallitems');
        exit;
    }

    function insert() {
        $this->isLoggedIn();
        $db = $this->getApp()->getDb();
        if (!$this->getApp()->isAdmin()) {
            App\WebApplication::alertMessage('You do not have permissions for this action.', 'warning');
            header('Location: /');
            exit;
        }

        $view = 'MenuItemEdit';
        $view_formdata = false;
        include_once _APP_LOC . '/admin/template/index.php';

        return true;
    }

    function edit() {
        $db = $this->getApp()->getDb();
        $this->isLoggedIn();
        if (!$this->getApp()->isAdmin()) {
            App\WebApplication::alertMessage('You do not have permissions for this action.', 'warning');
            header('Location: /');
            exit;
        }
        $model = new MenuItemModel($db);
        $view = 'MenuItemEdit';

        $view_formdata = $model->getItem($_GET['id']);
        include_once _APP_LOC . '/admin/template/index.php';

        return true;
    }

    function updateItem () {
        $db = $this->getApp()->getDb();
        $this->isLoggedIn();
        if (!$this->getApp()->isAdmin()) {
            App\WebApplication::alertMessage('You do not have permissions for this action.', 'warning');
            header('Location: /');
            exit;
        }
        $userId = $this->getApp()->getSession()->get('user.id');
        $model = new MenuItemModel($db);

        $model->name = $_POST['name'];
        $model->id = $_POST['id'];
        $model->url = $_POST['url'];

        if ($_POST['alias'] == '') {
            $model->alias = createAlias($_POST['name']);
        }
        else {
            $model->alias = createAlias($_POST['alias']);
        }

        $model->createdBy = $userId;

        if($model->updateItem()) {
            WebApplication::alertMessage('Item was successfully updated.', 'success');
        };

        $view_items = $model->getAllItems();
        $view = 'MenuItemList';

        include_once _APP_LOC . '/admin/template/index.php';

        return true;
    }



    function getAllItems() {
        $db = $this->getApp()->getDb();
        $this->isLoggedIn();
        if (!$this->getApp()->isAdmin()) {
            App\WebApplication::alertMessage('You do not have permissions for this action.', 'warning');
            header('Location: /');
            exit;
        }
        $model = new MenuItemModel($db);

        $view_items = $model->getAllItems();

        $view = 'MenuItemList';
        include_once _APP_LOC . '/admin/template/index.php';

        return true;
    }

    function deleteMenuItem () {
        $this->isLoggedIn();
        $db = $this->getApp()->getDb();
        if (!$this->getApp()->isAdmin()) {
            App\WebApplication::alertMessage('You do not have permissions for this action.', 'warning');
            header('Location: /');
            exit;
        }

        $model = new MenuItemModel($db);

        $model->id = $_GET['id'];
        $model->deleteItem();

        App\WebApplication::alertMessage('Menu item was successfuly deleted', 'success');


        $view_items = $model->getAllItems();

        $view = 'MenuItemList';
        include_once _APP_LOC . '/admin/template/index.php';

        return true;
    }

    function moveUp() {
        $this->isLoggedIn();
        $db = $this->getApp()->getDb();
        if (!$this->getApp()->isAdmin()) {
            App\WebApplication::alertMessage('You do not have permissions for this action.', 'warning');
            header('Location: /');
            exit;
        }

        $model = new MenuItemModel($db);
        $model->id = $_GET['id'];
        $model->moveUp();

        $view_items = $model->getAllItems('menu_item');

        header('Location: /menu/getallitems');
        exit;
    }

    function moveDown() {
        $this->isLoggedIn();
        $db = $this->getApp()->getDb();
        if (!$this->getApp()->isAdmin()) {
            App\WebApplication::alertMessage('You do not have permissions for this action.', 'warning');
            header('Location: /');
            exit;
        }

        $model = new MenuItemModel($db);
        $model->id = $_GET['id'];
        $model->moveDown();

        header('Location: /menu/getallitems');
        exit;
    }

    function getMenuItem() {
        $this->isLoggedIn();
        $db = $this->getApp()->getDb();
        if (!$this->getApp()->isAdmin()) {
            App\WebApplication::alertMessage('You do not have permissions for this action.', 'warning');
            header('Location: /');
            exit;
        }

        $model = new MenuItemModel($db);


        $view_formdata = $model->getItem($_GET['id']);


        $view = $this->getControllerName() . 'ItemEdit';

        include_once '/admin/template/index.php';

        return true;
    }



}


