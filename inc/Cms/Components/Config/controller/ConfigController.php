<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 12/07/2019
 * Time: 20:29
 */
namespace App\Cms\Components\Config\Controller;
defined('_APP_EXEC') or die;
use Joomla\Application\AbstractWebApplication;
use Joomla\Input\Input;
use Joomla\Renderer\RendererInterface;
use App\Base\Controller;
use App\Base\Controller\AbstractAppController;
use App\Base\Model;
use App\Cms\Components\Config\Model\ConfigModel;
use App;
class ConfigController extends AbstractAppController {
    /**
     * The template renderer.
     *
     * @var  RendererInterface
     */
    private $renderer;
    /**
     * The view object.
     *
     * @var  ContributorHtmlView
     */
    private $view;

    function __construct(Input $input, AbstractWebApplication $app) {
        parent::__construct($input, $app);
        $this->client = 'admin';
    }


    function set () {
        $db = $this->getApp()->getDb();

            $this->isLoggedIn();

        if (!$this->getApp()->isAdmin()) {
            App\WebApplication::alertMessage('You do not have permissions for this action.', 'warning');
            header('Location: /');
            exit;
        }

            $model = new ConfigModel($db);
            $model->title = $_POST['title'];
            $model->meta_description = $_POST['meta-description'];
            $model->meta_keywords = $_POST['meta-keywords'];
            $model->base_url = $_POST['base-url'];
            $model->database = [];
            $model->database['host'] = $_POST['database-host'];
            $model->database['database'] = $_POST['database-name'];
            $model->database['password'] = $_POST['database-password'];
            $model->database['user'] = $_POST['database-user'];
            $model->database['driver'] = $_POST['database-driver'];
            $model->database['prefix'] = $_POST['database-prefix'];
            $model->filesystem = [];
            $model->filesystem['uploads_directory'] = $_POST['uploads-directory'];


            if($model->set()) {
                App\WebApplication::alertMessage('Configuration successfully saved.', 'success');
            };

            $view_items = $model->get();

            $view = 'Configuration';
            include_once _APP_LOC . '/inc/Admin/template/index.php';

    }

    function get () {
        $db = $this->getApp()->getDb();
        $this->isLoggedIn();

        if (!$this->getApp()->isAdmin()) {
            App\WebApplication::alertMessage('You do not have permissions for this action.', 'warning');
            header('Location: /');
            exit;
        }

        $model = new ConfigModel($db);

        $model->id = $_GET['id'];
        $view_formdata = $model->get();

        $view = 'Configuration';
        include_once _APP_LOC . '/admin/template/index.php';
    }

    function isLoggedIn() {
        $userId = $this->getApp()->getSession()->get('user.id');

        if (!$userId) {

            header("Location: /user/login");

            exit;
        }

        return true;
    }

}