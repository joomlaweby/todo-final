<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 14/07/2019
 * Time: 16:52
 */
namespace App\Cms\Components\Config\Model;
defined('_APP_EXEC') or die;
use App\Base\Model;
use Joomla\Utilities\ArrayHelper;

class ConfigModel {


    function set() {
        $array = $this;
        $json = json_encode($array);

        header('Location: /config/get');
        exit;
    }

    function get() {
// Load the configuration file into an object.
        return json_decode(file_get_contents(JPATH_CONFIGURATION . '/config.json'));
    }
}