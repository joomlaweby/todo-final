<?php
namespace App\Cms\Components\Tag\Controller;
defined('_APP_EXEC') or die;
use Joomla\Application\AbstractWebApplication;
use Joomla\Input\Input;
use Joomla\Renderer\RendererInterface;
use App\Base\Controller;
use App\Base\Controller\AbstractAppController;
use App\Base\Model;
use App\Cms\Components\Tag\Model\TagModel;
use App;
use Joomla\DI\Container;
use App\WebApplication;
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 26/07/2019
 * Time: 00:33
 */
class TagController extends AbstractAppController
{
    /**
     * The view object.
     *
     * @var  ContributorHtmlView
     */
    private $view;
    /**
     * The template renderer.
     *
     * @var  RendererInterface
     */
    private $renderer;
    /**
     * Constructor.
     *
     */
    function __construct(Input $input, AbstractWebApplication $app) {
        parent::__construct($input, $app);
        $this->client = 'admin';
    }


    function insert() {
        $this->isLoggedIn();

        if (!$this->getApp()->isAdmin()) {
            WebApplication::alertMessage('You do not have permissions for this action.', 'warning');
            header('Location: /');
            exit;
        }
        $view_formdata = false;
        $view = 'TagEdit';
        include_once _APP_LOC . '/admin/template/index.php';

        return true;
    }


}