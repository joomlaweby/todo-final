<?php
namespace App\Cms\Components\Tag\Model;
defined('_APP_EXEC') or die;
use Joomla\Database;
use Joomla\Database\DatabaseDriver;
use App\Base\Model;
use App\Base\Model\AbstractItemModel;
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 26/07/2019
 * Time: 00:24
 */
class TagModel extends AbstractItemModel
{
    /**
     * Metadata description
     *
     * @var    string
     * @since  1.0
     */
    public $metadesc = null;

    /**
     * Key words for metadata
     *
     * @var    string
     * @since  1.0
     */
    public $metakey = null;

    /**
     * JSON string of other metadata
     *
     * @var    string
     * @since  1.0
     */
    public $metadata = null;
    function __construct(DatabaseDriver $db = null)
    {
        $this->table = $this->getDbTableName();
        parent::__construct($db);

    }
}