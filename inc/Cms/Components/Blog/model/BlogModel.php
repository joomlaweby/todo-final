<?php
namespace App\Cms\Components\Blog\Model;
defined('_APP_EXEC') or die;
use Joomla\Database;
use Joomla\Database\DatabaseDriver;
use App\Base\Model;
use App\Base\Model\AbstractItemModel;
use App;
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 23/07/2019
 * Time: 23:30
 */
class BlogModel extends AbstractItemModel
{
    /**
     * Category
     *
     * @var    int
     * @since  1.0
     */
    public $category;
    public $catid;
    public $category_name; // Deprecated

    /**
     * Metadata description
     *
     * @var    string
     * @since  1.0
     */
    public $metadesc = null;

    /**
     * Key words for metadata
     *
     * @var    string
     * @since  1.0
     */
    public $metakey = null;

    /**
     * JSON string of other metadata
     *
     * @var    string
     * @since  1.0
     */
    public $metadata = null;

    /**
     * Text of article
     *
     * @var    string
     * @since  1.0
     */
    public $text = null;

    function __construct(DatabaseDriver $db)
    {
        $this->table = $this->getDbTableName();
        parent::__construct($db);
    }

    /**
     * Makes author relation
     *
     * @return \Joomla\Entity\Relations\BelongsTo
     */
    public function author () {
        return $this->belongsTo('App\\Cms\\Components\\User\\Model\\UserModel', 'author', 'user_id');
    }

    /**
     * Makes author relation
     *
     * @return \Joomla\Entity\Relations\BelongsTo
     */
    public function category () {
        return $this->belongsTo('App\\Cms\\Components\\User\\Model\\UserModel', 'category', 'catid');
    }

    function insertItem() {

        $table = $this->getDbTableName();
        $this->ordering = 1;
        $sql = "SELECT MAX(ordering) as max_ordering FROM $table";

        $db = $this->db;

        $result = $db->setQuery($sql)->loadObject();


        if ($result != false) {
            $this->ordering = $result->max_ordering + 1;
        }



        $sql = "INSERT INTO $table (name, alias, text, catid, tags, ordering, created_by, created_at, updated_at, published)
            VALUES ('$this->name', '$this->alias', '$this->text', '$this->category', '$this->tags', $this->ordering, $this->createdBy, NOW(), NOW(), '1')";


        return $db->setQuery($sql)->execute();
    }


}