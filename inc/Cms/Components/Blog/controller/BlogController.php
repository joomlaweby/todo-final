<?php
namespace App\Cms\Components\Blog\Controller;
defined('_APP_EXEC') or die;
use App\Controller;
use App\Base\Controller\AbstractAppController;
use App\Cms\Components\Category\Controller\CategoryController;
use App\Cms\Components\Tag\Controller\TagController;
use App\Base\Model;
use App\Cms\Components\Blog\Model\BlogModel;
use App;
use App\WebApplication;
use Joomla\DI\Container;
use Joomla\Input;
use Joomla\Filter;
use Joomla\Filter\InputFilter;
use Joomla\Registry\Registry;
use Joomla\Application\AbstractApplication;
use Joomla\DI\ContainerAwareInterface;
use App\Base\Interfaces\EditableInterface;
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 23/07/2019
 * Time: 23:27
 */
class BlogController extends AbstractAppController  implements ContainerAwareInterface, EditableInterface
{

    function insert() {
        $this->isLoggedIn();
        if (!$this->getApp()->isAdmin()) {

            WebApplication::alertMessage('You do not have permissions for this action.', 'warning');
            header('Location: /');
            exit;
        }

        $category = new CategoryController($this->getInput(), $this->getApp());

        WebApplication::$viewData->categories = $category->getAll();

        $tag = new TagController($this->getInput(), $this->getApp());

        WebApplication::$viewData->tags = $tag->getAll();

        $view = 'BlogEdit';
        include_once _APP_LOC . '/admin/template/index.php';

        return true;
    }

    function insertItem() {

        $this->isLoggedIn();
        $db = $this->getApp()->getDb();
        if (!$this->getApp()->isAdmin()) {
            WebApplication::alertMessage('You do not have permissions for this action.', 'warning');
            header('Location: /');
            exit;
        }
        $userId = $this->getApp()->getSession()->get('user.id');
        /*
         * Todo: sanitize input with input filter
         */

        $input = $this->getInput();

        $model = new BlogModel($db);
        $model->name = trim($input->getString('name'));
        $model->category = $input->getInt('category');
        $model->text = trim($input->getHtml('text'));
        $model->tags = json_encode($input->getString('tags'));

        if ($input->get('alias') != '') {
            $model->alias = createAlias(trim($input->get('alias')));
        }
        else {
            $model->alias = createAlias($model->name);
        }

        $model->createdBy = $userId;



        /* Get list of categories */
        $model->parent = $input->getInt('parent-category');


        if($model->insertItem('blog')) {
            WebApplication::alertMessage('Article ' . $model->name . ' was successfully added.', 'success');
        };

        header('Location: /blog/getallitems');
        exit;
    }

    function getItem() {
        $this->isLoggedIn();
        $db = $this->getApp()->getDb();
        $input = $this->getInput();
        $model = new BlogModel($db);
        $view_items = $model->getItem($_GET['id']);

        $view = $this->getControllerName() . 'Edit';

        $category = new CategoryController($input, $this->getApp());

        WebApplication::$viewData->categories = $category->getAll();

        include_once _APP_LOC . '/admin/template/index.php';

        return true;
    }


    function moveUp() {
        $this->isLoggedIn();
        $db = $this->getApp()->getDb();
        if (!$this->getApp()->isAdmin()) {
            WebApplication::alertMessage('You do not have permissions for this action.', 'warning');
            header('Location: /');
            exit;
        }

        $model = new BlogModel($db);
        $model->id = $_GET['id'];
        $model->moveUp();

        $view_items = $model->getAllItems();

        header('Location: /blog/getallitems');
        exit;
    }

    function moveDown() {
        $this->isLoggedIn();
        $db = $this->getApp()->getDb();
        if (!$this->getApp()->isAdmin()) {
            WebApplication::alertMessage('You do not have permissions for this action.', 'warning');
            header('Location: /');
            exit;
        }

        $model = new BlogModel($db);
        $model->id = $_GET['id'];
        $model->moveDown();

        header('Location: /blog/getallitems');
        exit;
    }

    function getAllItems() {
        $db = $this->getApp()->getDb();

        $this->isLoggedIn();
        $model = new BlogModel($db);

        $view_items = $model->getAllItems($this->table_name);

        $view = 'BlogList';

        include_once _APP_LOC . '/admin/template/index.php';
        return true;

        }

    function getAll() {
        $db = $this->getApp()->getDb();
        $this->isLoggedIn();
        $model = new BlogModel($db);

        $view_items = $model->getAllItems($this->table_name);

        $view = 'BlogList';

        include_once _APP_LOC . '/inc/template/index.php';
        return true;

    }

    public function save() {

    }

    public function saveAndClose() {

    }

    public function cancel() {
        header('Location: /blog/getallitems');
        exit;
    }
}