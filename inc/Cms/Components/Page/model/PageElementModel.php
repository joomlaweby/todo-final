<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 31/07/2019
 * Time: 22:42
 */
namespace App\Cms\Components\Page\Model;

defined('_APP_EXEC') or die;

require_once _APP_LOC . '/inc/Cms/base/models/AbstractItemModel.php';

use App\Base\Model\AbstractItemModel;
use Joomla\Database\DatabaseDriver;
use Joomla\Model\DatabaseModelInterface;
use Joomla\Model\DatabaseModelTrait;
class PageElementModel extends AbstractItemModel
{
    function __construct($db = null)
    {
        $this->table = $this->getDbTableName();
        parent::__construct($db);
    }
}