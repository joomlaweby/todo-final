<?php
namespace App\Cms\Components\Page\Model;
defined('_APP_EXEC') or die;
use App\Base\Model\AbstractItemModel;
use Joomla\Database\DatabaseDriver;
use Joomla\Model\DatabaseModelInterface;
use Joomla\Model\DatabaseModelTrait;
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 24/07/2019
 * Time: 23:38
 */
class PageModel extends AbstractItemModel implements DatabaseModelInterface
{
    use DatabaseModelTrait;
    public $category;
    public $layout = [];
    /**
     * Metadata description
     *
     * @var    string
     * @since  1.0
     */
    public $metadesc = null;

    /**
     * Key words for metadata
     *
     * @var    string
     * @since  1.0
     */
    public $metakey = null;

    /**
     * JSON string of other metadata
     *
     * @var    string
     * @since  1.0
     */
    public $metadata = null;
    function __construct(DatabaseDriver $db)
    {
        $this->table = $this->getDbTableName();
        parent::__construct($db);

    }

    function addSection ($parent = 0) {

    }

    function insertItem() {

        $table = $this->getTableName();
        $db = $this->db;

        $sql = "SELECT MAX(ordering) as max_ordering FROM $table";

        $result = $db->setQuery($sql)->loadResult();

        $this->ordering = $result + 1;

        $sql = "INSERT INTO $table (name, ordering, alias, layout, created_at, published, updated_at, created_by, cat_id, meta_data, params)
            VALUES ('$this->name', $this->ordering, '$this->alias', '$this->layout', NOW(), $this->published, $this->updatedAt, $this->createdBy, '$this->category', '$this->metadata', '$this->params')";

        return $db->setQuery($sql)->execute();

    }
}