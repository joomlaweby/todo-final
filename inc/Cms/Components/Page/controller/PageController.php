<?php
namespace App\Cms\Components\Page\Controller;
defined('_APP_EXEC') or die;
use Joomla\Application\AbstractApplication;
use Joomla\Controller\AbstractController;
use Joomla\Input\Input;
use Joomla\Renderer\RendererInterface;
use App\Controller;
use App\WebApplication;
use App\Base\Controller\AbstractAppController;
use App\Cms\Components\Category\Controller\CategoryController;
use App\Base\Model;
use App\Cms\Components\Page\Model\PageModel;
use App;
use Joomla\DI\Container;
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 24/07/2019
 * Time: 23:36
 */
class PageController extends AbstractAppController
{
    /**
     * The view object.
     *
     * @var  ContributorHtmlView
     */
    private $view;
    /**
     * The template renderer.
     *
     * @var  RendererInterface
     */
    private $renderer;
    /**
     * Constructor.
     *
     */

    function __construct($input, $app) {
        parent::__construct($input, $app);
        $this->client = 'admin';

    }



    function insert() {
        $this->isLoggedIn();
        $input = $this->getInput();
        if (!$this->getApp()->isAdmin()) {
            WebApplication::alertMessage('You do not have permissions for this action.', 'warning');
            header('Location: /');
            exit;
        }

        $category = new CategoryController($input, $this->getApp());

        WebApplication::$viewData->categories = $category->getAll();



        $view_formdata = false;
        $view = 'PageEdit';
        include_once _APP_LOC . '/admin/template/index.php';

        return true;
    }

    function edit () {

        /* Access rights check */

        $this->isLoggedIn();
        if (!$this->getApp()->isAdmin()) {
            WebApplication::alertMessage('You do not have permissions for this action.', 'warning');
            header('Location: /');
            exit;
        }

        $db = $this->getApp()->getDb();
        $model = new PageModel($db);

        $view = 'PageEdit';
        include_once _APP_LOC . '/admin/template/index.php';

        return true;
    }
    function editLayout () {
        /* Access rights check */

        $this->isLoggedIn();
        if (!$this->getApp()->isAdmin()) {
            WebApplication::alertMessage('You do not have permissions for this action.', 'warning');
            header('Location: /');
            exit;
        }

        $db = $this->getApp()->getDb();
        $model = new PageModel($db);
        $model->id = $_GET['id'];


        $view_items = $model->getItem($model->id);

        $view = 'PageLayoutEdit';
        include_once _APP_LOC . '/admin/template/index.php';

        return true;
    }

    function addSection () {
        /* Access rights check */

        $this->isLoggedIn();
        if (!$this->getApp()->isAdmin()) {
            WebApplication::alertMessage('You do not have permissions for this action.', 'warning');
            header('Location: /');
            exit;
        }
        $db = $this->getApp()->getDb();
        $model = new PageModel($db);
        $model->id = $_GET['id'];
        $view_formdata = false;
        $view = 'PageElementEdit';
        include_once _APP_LOC . '/admin/template/index.php';

        return true;

    }

}