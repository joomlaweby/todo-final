<?php

defined('_APP_EXEC') or die;
require_once "model/PageModel.php";
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 27/07/2019
 * Time: 15:43
 */
class PageElementModel
{
    public $id;
    public $name;
    public $parent;
    public $content;
    public $type;
    public $ordering;
    public $allowed;
    public $published;

    function insert () {

        /* If no parent set, make element root level */
        if (!isset($parent) or $parent == '') {
            $this->parent = 0;
        }


    }

    function delete () {

    }

    function update () {

    }

    function publish () {

    }

    function unpublish () {

    }

    function moveUp () {

    }

    function moveDown () {

    }
}