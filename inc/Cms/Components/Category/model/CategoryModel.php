<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 14/07/2019
 * Time: 22:50
 */
namespace App\Cms\Components\Category\Model;
defined('_APP_EXEC') or die;
use Joomla\Database;
use Joomla\Database\DatabaseDriver;
use App\Base\Model;
use App\Base\Model\AbstractItemModel;
class CategoryModel extends AbstractItemModel {

    public $parent;
    public $level;
    public $context;
    public $alias;
    /**
     * Metadata description
     *
     * @var    string
     * @since  1.0
     */
    public $metadesc = null;

    /**
     * Key words for metadata
     *
     * @var    string
     * @since  1.0
     */
    public $metakey = null;

    /**
     * JSON string of other metadata
     *
     * @var    string
     * @since  1.0
     */
    public $metadata = null;

    function __construct(DatabaseDriver $db = null)
    {
        $this->table = $this->getDbTableName();
        parent::__construct($db);
    }

    function insertItem() {
        $db = $this->db;
        $table = $this->getDbTableName();

        $sql = "SELECT MAX(ordering) as max_ordering FROM $table";

        $max_ordering = $db->setQuery($sql)->loadResult();

        $this->ordering = $max_ordering + 1;

        $sql = "INSERT INTO $table (name, alias, parent, context, ordering, created_by, created_at, updated_at, published)
            VALUES ('$this->name', '$this->alias', '$this->parent', '$this->context', $this->ordering, $this->createdBy, NOW(), NOW(), '1')";


        return $db->setQuery($sql)->execute();
    }

    function updateItem () {
        $db = $this->db;
        $table = $this->getDbTableName();

        $sql = "UPDATE $table
                SET name = '$this->name',
                    parent = '$this->parent',
                    alias = '$this->alias',
                    updated_at = NOW()
                WHERE id = $this->id";

        return $db->setQuery($sql)->execute();
    }
}