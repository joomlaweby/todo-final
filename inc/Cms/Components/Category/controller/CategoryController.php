<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 14/07/2019
 * Time: 22:48
 */
namespace App\Cms\Components\Category\Controller;
defined('_APP_EXEC') or die;
use Joomla\Application\AbstractApplication;
use Joomla\Controller\AbstractController;
use Joomla\Input\Input;
use App\Base\Controller;
use App\Base\Controller\AbstractAppController;
use App\Base\Model;
use App\Cms\Components\Category\Model\CategoryModel;
use App;
use App\WebApplication;
use Joomla\DI\Container;
class CategoryController extends AbstractAppController {

    /**
     * The template renderer.
     *
     * @var  RendererInterface
     */
    private $renderer;
    /**
     * The view object.
     *
     * @var  ContributorHtmlView
     */
    private $view;

    function __construct($input, $app) {
        parent::__construct($input, $app);
        $this->client = 'admin';
    }



    function insert() {
        $this->isLoggedIn();

        if (!$this->getApp()->isAdmin()) {
            WebApplication::alertMessage('You do not have permissions for this action.', 'warning');
            header('Location: /');
            exit;
        }

        WebApplication::$viewData->categories = $this->getAll();

        $view = 'CategoryEdit';
        include_once _APP_LOC . '/admin/template/index.php';

        return true;
    }

    function insertItem() {
        $db = $this->getApp()->getDb();
        $this->isLoggedIn();

        if (!$this->getApp()->isAdmin()) {
            WebApplication::alertMessage('You do not have permissions for this action.', 'warning');
            header('Location: /');
            exit;
        }

        $model = new CategoryModel($db);

        $model->name = $_POST['name'];
        $model->parent = $_POST['parent-category'];
        if ($_POST['alias'] != '') {
            $model->alias = createAlias($_POST['alias']);
        }
        else {
            $model->alias = createAlias($model->name);
        }

        $model->createdBy = $this->getApp()->getSession()->get('user.id');
        $model->context = '';


        if($model->insertItem()) {
            WebApplication::alertMessage('Category ' . $model->name . ' was successfully added.', 'success');
        };
        /* Get list of categories */
        $model->parent = $_POST['parent-category'];


        header('Location: /category/getallitems');
        exit;
    }

    function updateItem () {
        $this->isLoggedIn();
        $db = $this->getApp()->getDb();
        if (!$this->getApp()->isAdmin()) {
            WebApplication::alertMessage('You do not have permissions for this action.', 'warning');
            header('Location: /');
            exit;
        }
        $model = new CategoryModel($db);
        $model->id = $_POST['id'];
        $model->name = $_POST['name'];

        $model->parent = $_POST['parent-category'];

        if ($_POST['alias'] == '') {
            $model->alias = createAlias($_POST['name']);
        }
        else {
            $model->alias = createAlias($_POST['alias']);
        }

        if($model->updateItem()) {
            WebApplication::alertMessage('Item was successfully updated.', 'success');
        };

        header('Location: /category/getallitems');
        exit;
    }

    function getItem() {
        $this->isLoggedIn();
        $db = $this->getApp()->getDb();
        $model_name = 'App\\Cms\\Components\\' . $this->getControllerName(). '\\Model\\' . $this->getControllerName(). 'Model';
        $table_name = $this->getTableName();

        $model = new $model_name($db);
        $view_items = $model->getItem($_GET['id']);

        $view = $this->getControllerName() . 'Edit';

        WebApplication::$viewData->categories = $this->getAll();

        if ($this->client == 'admin') {

            include_once _APP_LOC . '/admin/template/index.php';
        } else {
            include_once _APP_LOC . '/inc/template/index.php';
        }

        return true;
    }


}