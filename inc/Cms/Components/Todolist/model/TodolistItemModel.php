<?php
namespace App\Cms\Components\Todolist\Model;
defined('_APP_EXEC') or die;
use Joomla\Database;
use Joomla\Database\DatabaseDriver;
use App\Base\AbstractItemModel;
use App;
use Joomla\Model\DatabaseModelTrait;
class TodolistItemModel extends AbstractItemModel {
    use DatabaseModelTrait;


    public $date;
    public $file;
    public $apiKey;
    public $userId;
    public $status;
    public $priority;
    public $user_id;


    function __construct(DatabaseDriver $db = null)
    {
        $this->table = $this->getDbTableName();
        parent::__construct($db);
    }

    /**
     * Makes author relation
     *
     * @return \Joomla\Entity\Relations\BelongsTo
     */
    public function author () {
        return $this->belongsTo('App\\Cms\\Components\\User\\Model\\UserModel', 'author', 'user_id');
    }
    
    function getItem($id) {
        $db = $this->db;
        $table = $this->getDbTableName();
        
        $sql = "SELECT * FROM $table WHERE id = $id LIMIT 1";
        $result = $db->setQuery($sql)->loadObject();

        $this->id = $result->id;
        $this->name = $result->name;
        $this->date = $result->date;
        $this->file = $result->file;
        $this->ordering = $result->ordering;
        $this->createdAt = $result->created_at;
        $this->updatedAt = $result->updated_at;

        return true;
    }

    function insertItem() {
        $db = $this->db;
        $table = $this->getDbTableName();

        $user_table = '#__user';
        if (isset($this->apiKey)) {

            $this->apiKey = $db->escape($this->apiKey);
            $sql = "SELECT id FROM $user_table WHERE api_key = '$this->apiKey' LIMIT 1";

            return $db->setQuery($sql)->insertid();

        }

        $sql = "SELECT MAX(ordering) as max_ordering FROM $table";
        $result = $db->setQuery($sql)->loadResult();

        $this->ordering = $result + 1;

        $sql = "INSERT INTO $table (name, date, file, ordering, user_id, created_at, updated_at)
            VALUES ('$this->name', '$this->date', '$this->file', $this->ordering, $this->userId, NOW(), NULL )";

        return $db->setQuery($sql)->execute();
    }

    function updateItem() {
        $db = $this->db;
        $table = $this->getDbTableName();

        if (isset($this->apiKey)) {
            $sql = "UPDATE $table SET ";
            $sql .= isset($this->name) ? " name = '$this->name', " : "";
            $sql .= isset($this->date) ? " date = '$this->date', " : "";
            $sql .= isset($this->file) ? " file = '$this->file', " : "";
            $sql .= "updated_at = NOW() WHERE id = $this->id and user_id = $this->userId";

        }
        else {
            $sql = "UPDATE $table
                SET name = '$this->name',
                    date = '$this->date',
                    file = '$this->file',
                    updated_at = NOW()
                WHERE id = $this->id";
        }

        return $db->setQuery($sql)->execute();
    }



    function deleteItem() {
        $table = $this->getDbTableName();
        $db = $this->db;

        $query = $db->getQuery(true)
            ->select('file')
            ->from($db->quoteName($table))
            ->where('id = ' . $this->id);

        $result = $db->setQuery($query)->loadObject();

        $this->file = $result->file;

        if (file_exists($this->file)) {
            unlink($this->file);
        }

//        $this->delete();


        $query = $db->getQuery(true)
            ->delete()
            ->from($db->quoteName($table))
            ->where('id = ' . $this->id);

        $result = $db->setQuery($query)->execute();

        $this->consolidateItems();

        return true;
    }

    /**
     * @param $userId
     * @param null $apiKey
     * @return array   Returns array of items of user
     */
    function getAllItems() {
        $table = $this->getDbTableName();
        $apiKey = $this->apiKey;
        $db = $this->db;

        $user_table = '#__user';

        $query = $db->getQuery(true)
            ->select('i.id, i.name, i.date, i.file, i.ordering, i.created_at, i.updated_at, u.name as userName')
            ->from(($table . ' AS i') )
            ->join('LEFT', ($user_table . ' AS u'), 'i.user_id = u.id')
            ->where("i.user_id = $this->userId")
            ->where("u.published = 1")
            ->order("i.ordering")
            ->setLimit('50');

        $result = $db->setQuery($query)->loadObjectList();

        $r = [];
        if (count($result) > 0) {

            foreach ($result as $row)
            {
                $item = new TodolistItemModel($db);
                $item->id = $row->id;
                $item->name = $row->name;
                $item->date = substr($row->date, 0, -9);
                $item->file = $row->file;
                $item->ordering = $row->ordering;
                $item->createdAt = $row->created_at;
                $item->updatedAt = $row->updated_at;

                $r[] = $item;
            }
        }



        return $r;
    }

    function apiKeyIsValid($apiKey, $taskId) {
        $db = $this->db;
        $table = $this->getDbTableName();
        $apiKey = $db->escape($apiKey);

        $user_table = '#__user';
        if (isset($taskId)) {
            $sql = "SELECT i.id
                FROM $table i
                    LEFT JOIN $user_table u
                    ON i.User_id = u.ID
                WHERE u.api_key = '$apiKey'
                    AND u.ID = $taskId
                    ORDER BY i.Ordering ASC";
        }
        else {
            $sql = "SELECT id FROM $user_table WHERE api_key = '$apiKey' LIMIT 1";
        }

        $result = $db->setQuery($sql)->loadResult();

        if ($result != false && $result->num_rows > 0) {
            return true;
        }

        return false;
    }


    function deleteFile () {
        $db = $this->db;
        $table = $this->getDbTableName();

        $query = $db->getQuery(true)
            ->select('file')
            ->from($db->quoteName($table))
            ->where('id = ' . $this->id);

        $file = $db->setQuery($query)->loadResult()->file;

        $query = $db->getQuery(true)
            ->update($db->quoteName($table))
            ->set('file = ""')
            ->where('id = ' . $this->id);

        $db_success = $db->setQuery($query)->execute();

        if (file_exists($file)) {
            $success = unlink($file);
        }

        if (isset($success) and $success and $db_success) {
            return true;
        }
        elseif ($db_success) {
            return true;
        }
        else {
            return false;
        }
    }
}
