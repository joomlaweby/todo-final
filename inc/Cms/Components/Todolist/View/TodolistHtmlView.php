<?php
namespace App\Cms\Components\Todolist\View;
use App\Base\AppClasses\Document;
use App\Cms\Components\Todolist\Model\TodolistItemModel;
use App\WebApplication;
use Joomla\Renderer\RendererInterface;
use Joomla\Utilities\ArrayHelper;
use Joomla\View\HtmlView;

defined('_APP_EXEC') or die;


class TodolistHtmlView extends HtmlView
{
    public $model;

    /**
     * Instantiate the view.
     *
     * @param RendererInterface $renderer The renderer object.
     * @param Document $document Document object
     * @param TodolistItemModel $model
     */
    public function __construct(RendererInterface $renderer, TodolistItemModel $model)
    {
        parent::__construct($renderer);
        $this->model = $model;
    }

    /**
     * Method to render the view.
     *
     * @return  string  The rendered view.
     *
     * @since   1.0
     * @throws  \RuntimeException
     */
    public function render()
    {
        $this->model->userId = $_SESSION['user.id'];
        $this->setData(
            [
                'todolist' => ['title' => 'Todo-list app'],
                'document' => $this->document,
                'items' => ArrayHelper::fromObject($this->model->getAllItems())
            ]
        );

        return parent::render();
    }
}