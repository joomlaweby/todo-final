<?php
namespace App\Cms\Components\Todolist\Controller;
defined('_APP_EXEC') or die;
use App\Base\Controller;
use App\Base\Controller\AbstractAppController;
use App\Base\Model;
use App;
use App\WebApplication;
use Joomla\DI\Container;
use Joomla\Input\Input;
use Joomla\Registry;
use Joomla\Application\AbstractApplication;
use Joomla\DI\ContainerAwareInterface;
use App\Cms\Components\Todolist\Model\TodolistItemModel;
use App\Cms\Components\Todolist\View\TodolistHtmlView;
use App\Base\AppClasses\Document;
use Joomla\Utilities\ArrayHelper;

class TodolistController extends AbstractAppController  implements ContainerAwareInterface {

    /**
     * The template renderer.
     *
     * @var  RendererInterface
     */
    private $renderer;
    /**
     * The view object.
     *
     * @var  App\Cms\Components\Todolist\View\TodolistHtmlView
     */
    public $view;
    public $document;
    public $userId;

    public function __construct(Input $input, AbstractApplication $app)
    {
        $this->view = $app->getContainer()->get('view.todolist.html');
        $this->userId = $app->getSession()->get('user.id');

        parent::__construct($input, $app);
    }

    function getItem() {
        $db = $this->getApp()->getDb();
        $this->isLoggedIn();
        $input = $this->getInput();

        $model = new TodolistItemModel($db);
        $model->getItem($this->getIdFromUrl());

        $this->document->isEdit = true;
        $this->document->formData = ArrayHelper::fromObject($model);

        $this->view = $this->getApp()->getContainer()->get('view.todolist.html');
        $this->view->document = $this->document;
        return true;
    }

    function insertItem() {
        $db = $this->getApp()->getDb();
        $this->isLoggedIn();
        $input = $this->getInput();
        $userId = $this->getApp()->getSession()->get('user.id');
        $dir = $this->getApp()->getContainer()->get('config')->get('filesystem')->uploads_directory;

        //upload file
        $target_file = "";

        $timestamp = time();


        /**
         * @todo make function for upload file with filesystem package
         */

        if (isset($_FILES['fileUpload'])) {
            $target = $dir . $userId . '/';
            $target_file = $target . basename($_FILES['fileUpload']['name']);
            move_uploaded_file($_FILES['fileUpload']['tmp_name'], $target_file);
        }

        $model = new TodolistItemModel($db);
        $model->name = $input->get('task');
        $model->date = $input->get('date');
        $model->file = $target_file;
        $model->userId = $userId;

        if (isset($_POST['category'])) {
            $model->category = $_POST['category'];
        }


        if($model->insertItem()) {
            WebApplication::alertMessage('Item was successfully added.', 'success');
        };



        header("Location: /todolist/getallitems");
        exit;
    }

    function updateItem() {
        $db = $this->getApp()->getDb();
        $this->isLoggedIn();
        $userId = $this->getApp()->getSession()->get('user.id');
        $input = $this->getInput();
        $dir = $this->getApp()->getContainer()->get('config')->get('filesystem')->uploads_directory;
        //upload file
        $target_file = "";

        $model = new TodolistItemModel($db);


                
        if (isset($_FILES['fileUpload']) and $_FILES['fileUpload']['name'] != '') {
            $target = $dir . $userId . '/';
            $target_file = $target . basename($_FILES['fileUpload']['name']);
            move_uploaded_file($_FILES['fileUpload']['tmp_name'], $target_file);
            $model->file = $target_file;
        }
        else {
            $model->file = $input->post->get('file');
        }

        if ($input->post->get('file') != '') {
            $model->setAttribute('file', $input->post->get('file'));
        }

        if ($input->post->get('task') != '') {
            $model->setAttribute('name', $input->post->get('task'));
        }



        $model->id = $input->post->get('id');
        $model->name = $input->post->get('task');


        if ($input->post->get('date') != '') {
            $model->date = $input->post->get('date');
        }
        if($model->update()) {
            WebApplication::alertMessage('Item was successfully updated.', 'success');
        };

        
        header("Location: /todolist/getallitems");
        exit;
    }

    function moveUp() {
        $db = $this->getApp()->getDb();
        $this->isLoggedIn();

        $model = new TodolistItemModel($db);
        $model->id = $this->getIdFromUrl();
        $model->moveUp();

        header("Location: /todolist/getallitems");
        exit;
    }

    function moveDown() {
        $db = $this->getApp()->getDb();
        $this->isLoggedIn();
        
        $model = new TodolistItemModel($db);
        $model->id = $this->getIdFromUrl();

        $model->moveDown();

        header("Location: /todolist/getallitems");
        exit;
    }

    function deleteItem() {
        $db = $this->getApp()->getDb();
        $this->isLoggedIn();

        $model = new TodolistItemModel($db);
        $model->id = $this->getIdFromUrl();


        $model->deleteItem();

        WebApplication::alertMessage('Item was successfuly deleted', 'success');

        header("Location: /todolist/getallitems");
        exit;
    }

    function getAllItems() {
        $db = $this->getApp()->getDb();
        $this->isLoggedIn();
        $userId = $this->getApp()->getSession()->get('user.id');

        $model = new TodolistItemModel($db);
        $model->userId = $userId;
        $model->apiKey = null;

        $this->document = $this->getApp()->getDocument();
        $this->document->isEdit = false;


        $this->view = $this->getApp()->getContainer()->get('view.todolist.html');
        $this->view->document = $this->document;
        return true;
    }

    function deleteFile() {
        $db = $this->getApp()->getDb();
        $this->isLoggedIn();

        $model = new TodolistItemModel($db);
        $model->id = $this->getIdFromUrl();
        $model->deleteFile();

        WebApplication::alertMessage('File was successfuly deleted', 'success');

        header("Location: /todolist/getallitems");
        exit;
    }

}