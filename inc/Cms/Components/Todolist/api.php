<?php
namespace App\Controller;
defined('_APP_EXEC') or die;
require_once _APP_LOC . "/inc/Cms/model/TodolistItemModel.php";
use App\Model;
use App;
class ApiController {

    function verifyKey($apiKey, $taskId = null) {
        $model = new App\Model\TodolistItemModel();
        $apiKeyIsValid = $model->apiKeyIsValid($apiKey, $taskId);

        if (!$apiKeyIsValid) {
            echo "API Key is not valid.";
            exit;
        }
    }
    
    function getAllTasks() {

        if ($_SERVER['REQUEST_METHOD'] != 'GET') {
            echo "Method " . $_SERVER['REQUEST_METHOD'] . " is not allowed";
            exit;
        }

        $this->verifyKey($_GET['apiKey'] ?? "");

        $model = new App\Model\TodolistItemModel();
        $view_items = $model->getAllItems($_GET['apiKey']);

        echo json_encode($view_items);
        exit;
    }

    function createTask() {

        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            echo "Method " . $_SERVER['REQUEST_METHOD'] . " is not allowed";
            exit;
        }


        $this->verifyKey($_POST['apiKey']);

        //upload file
        $target_file = "";

        if (isset($_FILES['fileUpload'])) {
            $target = 'assets/';
            $target_file = $target . basename($_FILES['fileUpload']['name']);
            move_uploaded_file($_FILES['fileUpload']['tmp_name'], $target_file);
        }

        $model = new App\Model\TodolistItemModel();

        $model->name = $_POST['task'];
        $model->date = $_POST['date'];
        $model->file = $target_file;
        $model->apiKey = $_POST['apiKey'];

        $result = $model->insertItem();

        echo json_encode($result);
        exit;

    }

    function updateTask() {

        if ($_SERVER['REQUEST_METHOD'] != 'PUT') {
            echo "Method " . $_SERVER['REQUEST_METHOD'] . " is not allowed";
            exit;
        }

        parse_str(file_get_contents("php://input"), $_PUT);

        $this->verifyKey($_PUT['apiKey'], $_PUT['id']);
        //upload file
        $target_file = "";

        if (isset($_FILES['fileUpload'])) {
            $target = 'assets/';
            $target_file = $target . basename($_FILES['fileUpload']['name']);
            move_uploaded_file($_FILES['fileUpload']['tmp_name'], $target_file);
        }

        $model = new App\Model\TodolistItemModel();

        $model->id = $_PUT['id'];
        $model->name = $_PUT['task'] ?? null;
        $model->date = $_PUT['date'] ?? null;
        $model->file = $target_file;

        echo json_encode($model->updateItem());

    }

    function deleteTask() {

        if ($_SERVER['REQUEST_METHOD'] != 'DELETE') {
            echo "Method " . $_SERVER['REQUEST_METHOD'] . " is not allowed";
            exit;
        }

        parse_str(file_get_contents("php://input"), $_DELETE);

        $this->verifyKey($_DELETE['apiKey'], $_DELETE['id']);

        $model = new App\Model\TodolistItemModel();
        $model->id = $_DELETE['id'];

        $result = $model->deleteItem();

        echo json_encode($result);
        exit;
    }

    function moveUpTask() {
        if ($_SERVER['REQUEST_METHOD'] != "PUT" ) {
            echo "Method " . $_SERVER['REQUEST_METHOD'] . " is not allowed";
            exit;
        }
        parse_str(file_get_contents("php://input"), $_PUT);


        $this->verifyKey($_PUT['apiKey'], $_PUT['id']);

        $model = new App\Model\TodolistItemModel();
        $model->id = $_PUT['id'];

        echo json_encode($model->moveUp());
        exit;
    }

    function moveDownTask() {
        if ($_SERVER['REQUEST_METHOD'] != "PUT" ) {
            echo "Method " . $_SERVER['REQUEST_METHOD'] . " is not allowed";
            exit;
        }
        parse_str(file_get_contents("php://input"), $_PUT);


        $this->verifyKey($_PUT['apiKey'], $_PUT['id']);

        $model = new App\Model\TodolistItemModel();
        $model->id = $_PUT['id'];

        echo json_encode($model->moveDown());
        exit;
    }
}