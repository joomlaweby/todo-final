<?php
namespace App\Cms\Components\User\View;
use App\Base\AppClasses\Document;
use Joomla\Renderer\RendererInterface;
use Joomla\View\BaseHtmlView;

defined('_APP_EXEC') or die;

class LoginHtmlView extends BaseHtmlView
{
    /**
     * Instantiate the view.
     *
     * @param   RendererInterface  $renderer          The renderer object.
     * @param   Document $document                    Document object
     */
    public function __construct(RendererInterface $renderer, Document $document)
    {
        parent::__construct($renderer);
        $this->document = $document;
    }

    /**
     * Method to render the view.
     *
     * @return  string  The rendered view.
     *
     * @since   1.0
     * @throws  \RuntimeException
     */
    public function render()
    {

        $this->setData(
            [
                'document' => $this->document
            ]
        );

        return parent::render();
    }
}