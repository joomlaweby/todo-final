<?php


namespace App\Cms\Components\User\View;
use App\Base\AppClasses\Document;
use Joomla\Renderer\RendererInterface;
use Joomla\View\BaseHtmlView;
use App\Cms\Components\User\Model\UserModel;
defined('_APP_EXEC') or die;

class ProfileHtmlView extends BaseHtmlView
{
    /**
     * Instantiate the view.
     *
     * @param RendererInterface $renderer The renderer object.
     * @param Document $document Document object
     * @param UserModel $model
     */
    public function __construct(RendererInterface $renderer, Document $document, UserModel $model)
    {
        parent::__construct($renderer);
        $this->document = $document;
        $this->model = $model;
    }

    /**
     * Method to render the view.
     *
     * @return  string  The rendered view.
     *
     * @since   1.0
     * @throws  \RuntimeException
     */
    public function render()
    {

        $this->setData(
            [
                'document' => $this->document,
                'user' => $this->model->getSettings()
            ]
        );

        $this->setLayout('profile.index.twig');

        return parent::render();
    }
}