<?php
namespace App\Cms\Components\User\Controller;
defined('_APP_EXEC') or die;

use App\Cms\Components\User\View\LoginHtmlView;
use App\Base\Controller;
use App\Base\Controller\AbstractAppController;
use App\Base\Model;
use App\WebApplication;
use App\Cms\Components\User\Model\UserModel;
use App;
use Joomla\DI\Container;
use Joomla\Input\Input;
use Joomla\Registry;
use Joomla\Application\AbstractApplication;
use Joomla\DI\ContainerAwareInterface;
use Zend\Diactoros\Response\HtmlResponse;

class UserController extends AbstractAppController  implements ContainerAwareInterface{

    /**
     * The template renderer.
     *
     * @var  RendererInterface
     */
    private $renderer;
    /**
     * The view object.
     *
     * @var  ContributorHtmlView
     */
    private $view;

    /**
     * @param   HtmlView      $view   The view object.
     * @param   Input                $input  The input object.
     * @param   AbstractApplication  $app    The application object.
     * Constructor
     */

    public function __construct(Input $input, AbstractApplication $app, $view = null)
    {
        parent::__construct($input, $app);
        $this->renderer = [];
        $this->view = $view;
    }

    function login() {

        $view = 'UserLogin';

        include_once _APP_LOC . '/inc/Template/index.php';

        return true;
    }


    function loginSubmit() {

        $db = $this->getApp()->getDb();
        $session = $this->getApp()->getSession();
        $model = new UserModel($db);
        $model->username = $this->getInput()->get('username');
        $model->password = $this->getInput()->get('password');
        $model->rememberMe = $this->getInput()->get('remember-me');
        $model->input = $this->getInput();

        if ($model->verifyUser()) {
            $session->set('user.id', $model->id);
            $session->set('user.username', $model->username);
            $session->set('user.lastActivity', time());
            $session->set('user.rememberMe', $model->rememberMe);

            WebApplication::alertMessage('Login successful!', 'success');

            header("Location: /todolist/getallitems");
            exit;
        }

        WebApplication::alertMessage('Wrong credentials!', 'warning');
        header("Location: /user/login");
        exit;
    }

    function logoutSubmit() {

        $this->getApp()->getSession()->close();
        WebApplication::alertMessage('Logout successful!', 'success');
        header("Location: /user/login");
        exit;
    }

    function getAllUsers () {
        $db = $this->getApp()->getDb();
        $this->isLoggedIn();

        $model = new UserModel($db);

        $view_items = $model->getAllUsers();

        $view = 'UserList';

        include_once _APP_LOC . '/admin/template/index.php';

        return true;



    }

    /**
     * Show user profile settings
     *
     * @return bool
     */

    function settings () {
        $db = $this->getApp()->getDb();
        $this->isLoggedIn();
        $model = new UserModel($db);
        $model->id = $this->getApp()->getSession()->get('user.id');
        $view_items = $model->getSettings();

        $view = 'UserSettings';
        include_once _APP_LOC . '/inc/Template/index.php';

        return true;
    }

    function settingsSubmit () {
        $db = $this->getApp()->getDb();
        $this->isLoggedIn();
        $model = new UserModel($db);
        $userId = $this->getApp()->getSession()->get('user.id');
        $params = [];

        $params['style'] = $_POST['settings-choose-theme'];

        $model->params = json_encode($params);
        $model->id = $userId;

        if($model->updateSettings()) {
            WebApplication::alertMessage('Settings were successfully saved.', 'success');
        };

        header("Location: /todolist/getallitems");
        exit;
    }

    function deleteUser () {
        $config = $this->getConfig();
        $this->isLoggedIn();
        $db = $this->getApp()->getDb();
        $session = $this->getApp()->getSession();
        $userId = $session->get('user.id');
        /*
         * Check for admin rights
         */
        if (!$this->getApp()->isAdmin()) {
            WebApplication::alertMessage('You do not have permissions for this action.', 'warning');
            header('Location: /');
            exit;
        }

        /*
         * Check, if admin is not deleting himself
         */
        if ($userId == $this->getInput()->get('id')) {
            WebApplication::alertMessage('You can`t delete yourself!', 'warning');
            header("Location: /user/getallusers");
            exit;
        }

        $model = new UserModel($db);
        $dir = $config->get('filesystem')->uploads_directory;

        $model->id = $_GET['id'];

        if($model->deleteUser()) {
            if (is_dir($dir . $model->id)) {
                rmdir($dir . $model->id);
                WebApplication::alertMessage('User upload folder was successfuly deleted', 'info');
            }
            WebApplication::alertMessage('User was successfuly deleted', 'success');
        };



        header("Location: /user/getallusers");
        exit;
    }

    function register () {

        $view = 'UserRegister';
        include_once _APP_LOC . '/inc/Template/index.php';

        return true;
    }

    function registerSubmit () {
        $config = $this->getConfig();
        $db = $this->getApp()->getDb();
        $model = new UserModel($db);

        $params = [];

        $params['style'] = '1';

        $model->params = json_encode($params);

        if (isset($_POST['password'])
            and isset($_POST['verify-password'])
            and $_POST['password'] != $_POST['verify-password'])
        {

            WebApplication::alertMessage('Passwords didn`t match', 'warning');

            $view_data = new \stdClass();

            $view_data->username = $_POST['username'];
            $view = 'UserRegister';
            include_once _APP_LOC . '/inc/Template/index.php';

            return true;
        }

        if (isset($_POST['username']) and $_POST['username'] != '') {
            $model->username = $_POST['username'];
        }

        if (isset($_POST['password']) and $_POST['password'] != '') {
            $model->password = sha1($_POST['password']);
        }



        $model->apiKey = $model->generateApiKey();
        $model->published = 1;
        $model->isAdmin = 0;
        $id = $model->registerUser();
        if ($id) {
            WebApplication::alertMessage('Account successfully created', 'success');
        }

        $dir = $config->get('filesystem')->uploads_directory;

        /* Todo: fix $id */

        if (mkdir( $dir . $id )) {
            WebApplication::alertMessage('User folder created', 'info');
        }


        header("Location: /todolist/getallitems");
        exit;


    }

    function moveUp() {
        $db = $this->getApp()->getDb();
        $this->isLoggedIn();
        $input = $this->getApp()->input;
        if (!$this->getApp()->isAdmin()) {
            WebApplication::alertMessage('You do not have permissions for this action.', 'warning');
            header('Location: /');
            exit;
        }

        $model = new UserModel($db);
        $model->id = $input->get('id');
        $model->moveUp();

        header("Location: /user/getallusers");
        exit;
    }

    function moveDown() {
        $db = $this->getApp()->getDb();
        $this->isLoggedIn();

        if (!$this->getApp()->isAdmin()) {
            WebApplication::alertMessage('You do not have permissions for this action.', 'warning');
            header('Location: /');
            exit;
        }

        $model = new UserModel($db);
        $model->id = $_GET['id'];
        $model->moveDown();

        header("Location: /user/getallusers");
        exit;
    }

    function insert () {
        $this->isLoggedIn();
        if (!$this->getApp()->isAdmin()) {
            WebApplication::alertMessage('You do not have permissions for this action.', 'warning');
            header('Location: /');
            exit;
        }

        $view = 'UserEdit';
        include_once _APP_LOC . '/admin/template/index.php';

        return true;
    }

    function getAllItems () {
        return $this->getAllUsers();
    }

    public function save() {

    }

    public function saveAndClose() {

    }

    public function cancel() {

    }

    /**
     * Execute the controller.
     *
     * @return  boolean
     */
    public function execute(): bool
    {
        $methodClass = $this->getMethod();

        // call method
        if ($methodClass != null) {
            $this->$methodClass();
        }

        // Enable browser caching
        $this->getApplication()->allowCache(true);

        // Do method stuff

        // Set response

        $this->getApplication()->setResponse(new HtmlResponse($this->view->render()));

        return true;
    }

}