<?php
namespace App\Cms\Components\User\Model;
defined('_APP_EXEC') or die;
use Joomla\Database;
use Joomla\Database\DatabaseDriver;
use Joomla\Authentication\Authentication;
use Joomla\Authentication\Strategies;
use Joomla\Input\Input;
use Joomla\Authentication\Strategies\DatabaseStrategy;

use App\Base\Model\AbstractItemModel;
use App;
class UserModel extends AbstractItemModel {
    public $username;
    public $password;
    public $avatar;
    public $published;
    public $isAdmin;
    public $apiKey;
    public $input;
    public $rememberMe;
    protected $role = 'guest';

    function __construct(DatabaseDriver $db)
    {
        $this->table = $this->getDbTableName();
        parent::__construct($db);

    }
    
    function verifyUser() {

        $table = $this->getDbTableName();
        $username= $this->username;

        $password = sha1($this->password);

        $db = $this->db;

        $sql = "SELECT id, username FROM $table WHERE username = '$username' AND password = '$password' AND published = 1 LIMIT 1";

        $result = $db->setQuery($sql)->loadObject();

        if (!$result) return false;

        $this->id = $result->id;
        $this->username = $result->username;

        return true;
    }

    /**
     * Authenticate user
     */
    function authenticate () {
        $authentication = new Authentication;

        $options = array(
            'database_table'  => '#__user', // Name of the database table the user data is stored in
            'username_column' => 'username', // Name of the column in the database containing usernames
            'password_column' => 'password', // Name of the column in the database containing passwords
        );

        $strategy = new DatabaseStrategy($this->input, DatabaseDriver::getInstance(), $options);

        $authentication->addStrategy('database', $strategy);

        $username = $authentication->authenticate(array('database'));
    }

    function getAllUsers ($apiKey = null) {

        $table = $this->getDbTableName();

        $db = $this->db;

        $iterator = $db->setQuery($db->getQuery(true)->select('*')->from($table)->order('ordering'))->getIterator();

        $items = [];

        if (count($iterator) > 0) {
            foreach ($iterator as $row)
            {
                $row = (array)$row;
                $item = new UserModel();
                $item->id = $row['id'];
                $item->username = $row['username'];
                $item->ordering = $row['ordering'];
                $item->published = $row['published'];
                $item->createdAt = $row['created_at'];
                $item->updatedAt = $row['updated_at'];
                $item->isAdmin = $row['is_admin'];
                $item->params = $row['params'];
                $items[] = $item;
            }
        }

        return $items;
    }

    function getSettings () {
        $table = $this->getDbTableName();

        $db = $this->db;
        $query = $db->getQuery(true)
            ->select('*')
            ->from($db->quoteName($table))
            ->where('id', $this->id);

        $result = $db->setQuery($query)->loadObject();

        return $result;

    }

    function updateSettings () {
        $table = $this->getDbTableName();

        $db = $this->db;
        
        $sql = "UPDATE $table SET
                    params = '$this->params',
                    updated_at = NOW()
                WHERE id = $this->id ";

        return $db->setQuery($sql)->execute();
    }

    function deleteUser() {
        $db = $this->db;

        $todo_list_table = '#__todolist_item';

        $table = $this->getDbTableName();
        $sql = "DELETE FROM $table WHERE id = $this->id " . ";";
        $db->setQuery($sql)->execute();

        $sql = "DELETE FROM $todo_list_table WHERE userID = $this->id";


        if($db->setQuery($sql)->execute())
        {
            $this->consolidateItems();

            return true;
        }
        else {
            return false;
        };


    }

   function registerUser() {

       $db = $this->db;

       $table = $this->getDbTableName();

       $sql = "SELECT MAX(ordering) as max_ordering FROM $table";

       $ordering = $db->setQuery($sql)->loadResult();

       $this->ordering = $ordering + 1;
       $this->name = '';

       $sql = "INSERT INTO $table (name, username, password, avatar, published, is_admin, api_key, params, created_at, ordering)
            VALUES ('$this->name', '$this->username', '$this->password', '$this->avatar',$this->published, $this->isAdmin, '$this->apiKey', '$this->params', NOW(), $this->ordering)";

       return $db->setQuery($sql)->execute()->id;

   }

    function generateApiKey () {
        return randString(100);
    }

    function moveUp() {
        $db = $this->db;
        $table = $this->getDbTableName();
        $sql = "SELECT ordering FROM $table WHERE id = $this->id LIMIT 1";
        $ordering = $db->setQuery($sql)->loadResult();

        if ($ordering == 1) {
            return true;
        }

        $sql = "UPDATE $table SET ordering = ordering + 1 WHERE ordering = " . ($ordering - 1) . ";";
        $db->setQuery($sql)->execute();
        $sql = "UPDATE $table SET ordering = ordering - 1 WHERE id = $this->id;";

        return $db->setQuery($sql)->execute();
    }

    function moveDown() {
        $db = $this->db;
        $table = $this->getDbTableName();
        $sql = "SELECT ordering FROM $table WHERE id = $this->id LIMIT 1";
        $ordering = $db->setQuery($sql)->loadResult();



        $sql = "SELECT MAX(ordering) as max_ordering FROM $table";
        $max_ordering = $db->setQuery($sql)->loadResult();


        if ($ordering == $max_ordering) {
            return true;
        }

        $sql = "UPDATE $table SET ordering = ordering - 1 WHERE ordering = " . ($ordering + 1) . ";";
        $db->setQuery($sql)->execute();
        $sql = "UPDATE $table SET ordering = ordering + 1 WHERE id = $this->id;";

        return $db->setQuery($sql)->execute();
    }

    function isAdmin () {
        $table = $this->getDbTableName();

        $sql = "SELECT is_admin FROM $table WHERE id = $this->id LIMIT 1";

        $db = $this->db;

        $result = $db->setQuery($sql)->loadResult();

        return $result;

    }


}
