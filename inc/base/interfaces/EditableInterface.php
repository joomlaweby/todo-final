<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 03/08/2019
 * Time: 14:13
 */

namespace App\Base\Interfaces;
defined('_APP_EXEC') or die;

interface EditableInterface
{
    public function save();

    public function saveAndClose();

    public function cancel();
}