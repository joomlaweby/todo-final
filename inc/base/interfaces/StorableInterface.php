<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 03/08/2019
 * Time: 14:15
 */

namespace App;
defined('_APP_EXEC') or die;

interface StorableInterface
{
    function updateItem();
}