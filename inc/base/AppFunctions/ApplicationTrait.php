<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 08/08/2019
 * Time: 00:11
 */

namespace App\Base\AppFunctions;
defined('_APP_EXEC') or die;

use App\Cms\Components\Chuck\Controller\ChuckController;
use App\WebApplication;
use Joomla\Database;
use App\DebugBar;
use Joomla\Router\Router;
use Joomla\Registry\Registry;
use Joomla\String\StringHelper;
use App\Service;
use Joomla\Utilities\ArrayHelper;
use App\Base\Model;
use App\Cms\Components\User\Model\UserModel;
use App\Base\Controller;
use App;
use Joomla\DI\Container;
use App\Cms\Components\User\Controller\UserController;
use App\Cms\Components\TodoList\Controller\TodoListController;
use App\Cms\Components\Config\Controller\ConfigController;
use Joomla\Router\Route;
use App\DebugBar\JoomlaHttpDriver;
use Joomla\Session\Session;
use Joomla\Uri\Uri;
use Joomla\Session\SessionInterface;
use Psr\Http\Message\ResponseInterface;
use Zend\Diactoros\Response;
use Zend\Diactoros\Stream;
trait ApplicationTrait
{
    /**
     * DI Container
     *
     * @var    Container
     * @since  1.0
     */
    protected $container;
    /**
     * The application document object.
     *
     * @var    Document
     * @since  1.0
     */
    protected $document;
    /**
     * A session object.
     *
     * @var    Session
     * @since  1.0
     * @note   This has been created to avoid a conflict with the $session member var from the parent class.
     */
    private $session = null;
    /**
     * The default theme.
     *
     * @var    string
     * @since  1.0
     */
    protected $theme = null;

    /**
     * The application language object.
     *
     * @var    Language
     * @since  1.0
     */
    protected $language;



    /**
     * Set the DI container.
     *
     * @param   Container  $container  The DI container.
     *
     * @return  $this  Method allows chaining
     *
     * @since   1.0
     */
    public function setContainer(Container $container)
    {
        $this->container = $container;

        return $this;
    }

    /**
     * Get the DI container.
     *
     * @return  Container
     *
     * @since   1.0
     */
    public function getContainer()
    {
        return $this->container;
    }





    /**
     * Adds alert message to array
     *
     * @param string $message
     *
     * @param string $class Bootstrap alert class
     */

    static function alertMessage ($message, $class = 'info') {
        if (!isset($_SESSION['systemMessages'])) {
            $_SESSION['systemMessages'] = [];
        }
        $_SESSION['systemMessages'][] = [
            'message' => $message,
            'style' => $class
        ];
    }

    /**
     * Renders array of system messages
     **/
    static function displaySystemMessages () {

        if (!isset($_SESSION['systemMessages'])) {
            return false;
        }
        $messages = $_SESSION['systemMessages'];

        foreach ($messages as $key => $item) {
            $message = $item['message'];
            $class = $item['style'];
            include(_APP_LOC . '/inc/Template/layouts/alert_message.php');
        }
        unset($_SESSION['systemMessages']);
        return true;
    }

    static function countDaysleft($date) {
        $dateTimestamp = strtotime($date);
        $nowTimestamp = time();
        $result = round((int)($dateTimestamp - $nowTimestamp)/(60 * 60 * 24)) +1;
        return $result;
    }

    /**
     * Get logged in user id
     *
     * @param
     *
     * @return int user id
     *
     **/
    function getLoggedInUser() {

        $session = $this->getSession();

        if ($session->get('user.id')) {
            return $session->get('user.id');
        }
        else {
            return 0;
        }
    }

    /**
     * Get a session object.
     *
     * @return  Session
     *
     * @since   1.0
     */
    public function getSession()
    {
        if (is_null($this->session))
        {
            $this->session = new Session;
            $this->session->start();
        }

        return $this->session;
    }

    function expiredSessionCheck () {
        $session = $this->getSession();



        if ($session->has('user.lastActivity')) {
            $lastActivity = $session->get('user.lastActivity');
            $rememberMe = $session->get('user.rememberMe');
            if (((time() - $lastActivity) /(60) > 10 ) and $rememberMe != 'on') {

                $this->getSession()->close();
                $this->alertMessage('Your session has been expired! You must login again.', 'primary');
                header("Location: /user/login");
                exit;
            }
            else {
                $session->set('user.lastActivity', time());
                return true;
            }
        }
    }

    /**
     * Check if user is logged in
     *
     * @return mixed true on success, else redirect
     **/

    function isLoggedIn() {
        $loggedIn = $this->getSession()->get('user');

        if (!$loggedIn) {
            self::alertMessage('You need to login!', 'warning');
            header("Location: /user/login");
            exit;
        }

        return true;
    }

    /**
     * Redirects to 404 error page
     *
     *
     **/

    function show_404() {
        header('HTTP/1.0 404 Not Found');
        $this->displaySystemMessages();
        include_once(_APP_LOC . '/inc/Template/layouts/404.php');
        die();
    }

    /**
     * Check, if logged in user is admin or not
     *
     * @return bool
     */

    function isAdmin () {
        $db = $this->getDb();

        $model = new UserModel($db);
        $model->id = $this->getSession()->get('user.id');

        if ($model->isAdmin() == '0') {
            return false;
        }

        elseif ($model->isAdmin() == '1') {
            return true;
        }

        return false;

    }

    /**
     * Set the application's debug bar
     *
     * @param   DebugBar  $debugBar  DebugBar object to set
     *
     * @return  $this
     */
    public function setDebugBar(DebugBar $debugBar): self
    {
        $this->debugBar = $debugBar;

        return $this;
    }

    /**
     * @return mixed DatabaseDriver
     */

    public function getDb () {
        return $this->db;
    }

    /**
     * Get application object
     * @return mixed Application object
     */
    public function getApp () {
        return $this;
    }


    /**
     *
     * Sets initial individual property value for document
     *
     * @param $property string   Name of property
     * @param $value string      Property value
     *
     * @return $this             Returns $this to support chaining
     *
     */
    public function setDocumentProperty ($property, $value) {
        $this->document->$property = $value;
        return $this;
    }

    /**
     * Get document object
     */
    public function getDocument () {
        return $this->document;
    }

    /**
    * Sets initial values for document from global settings
    */
    public function setDocument () {
        $this->document->set('title', $this->getContainer()->get('config')->get('metadata.title'));
        $this->document->set('meta', $this->getContainer()->get('config')->get('metadata'));
        $this->document->set('css', ['inc/Template/css/theme-3.css', 'inc/Template/css/main.css']);
        $this->document->set('js', ArrayHelper::toObject(['inc/Template/js/jquery.min.js', 'inc/Template/js/bootstrap.js', 'inc/Template/js/app.js']));
        $jokeController = new ChuckController;
        $view_joke = $jokeController->getJoke();
        $this->document->set('chuck', $view_joke);
        $this->document->set('url', 'http://'. $_SERVER['HTTP_HOST']);

//        $this->document->set('meta_keywords', $this->getContainer()->get('config')->get('metadata.met.keywords'));
    }

    function checkRoutes ($routes, $url) {

        foreach ($routes as $key => $route) {
            if (ArrayHelper::getValue($route, 'pattern') == $url) {
                return true;
            }
        }
        return false;
    }


    /**
     * Set the application's router
     *
     * @param   Router  $router  Router object to set
     *
     * @return  $this
     */
    public function setRouter(Router $router): self
    {
        $this->router = $router;

        return $this;
    }

}