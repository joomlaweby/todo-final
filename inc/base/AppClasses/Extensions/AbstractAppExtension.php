<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 06/08/2019
 * Time: 00:24
 */

namespace App\Base\Extensions;
defined('_APP_EXEC') or die;

Abstract class AbstractAppExtension
{
    public $id;
    public $systemName;
    public $type;
    public $author;
    public $version;
}