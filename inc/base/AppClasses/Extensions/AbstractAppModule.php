<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 06/08/2019
 * Time: 00:22
 */

namespace App\Base\Extensions;
defined('_APP_EXEC') or die;
use App\Base\Extensions\AbstractAppExtension;
Abstract class AbstractAppModule extends AbstractAppExtension
{
    public $position;
}