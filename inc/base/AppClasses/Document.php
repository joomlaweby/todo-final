<?php
namespace App\Base\AppClasses;
defined('_APP_EXEC') or die;
use Joomla\Registry\Registry;
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 25/07/2019
 * Time: 23:16
 */
class Document extends Registry
{
    public $title;
    public $mainContent;
    public $formData;
    public $modules;
    public $isEdit;
    /**
     * Contains the document language setting
     *
     * @var    string
     */
    public $language = 'en-gb';

    /**
     * Contains the character encoding string
     *
     * @var    string
     */
    public $charset = 'utf-8';
    /**
     * Array of linked scripts
     *
     * @var    array
     * @since  1.7.0
     */
    public $scripts = array();

    /**
     * Array of linked style sheets
     *
     * @var    array
     */
    public $styleSheets = array();

    /**
     * Array of included style declarations
     *
     * @var    array
     */
    public $style = array();

    /**
     * Array of meta tags
     *
     * @var    array
     */
    public $metaTags = array();

    /**
     * The rendering engine
     *
     * @var    object
     */
    public $engine = null;


    /**
     * Sets the global document language declaration. Default is English (en-gb).
     *
     * @param   string  $lang  The language to be set
     *
     * @return  Document instance of $this to allow chaining
     *
     */
    public function setLanguage($lang = 'en-gb')
    {
        $this->language = strtolower($lang);

        return $this;
    }

    /**
     * Returns the document language.
     *
     * @return  string
     *
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Sets the title of the document
     *
     * @param   string  $title  The title to be set
     *
     * @return  Document instance of $this to allow chaining
     *
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Return the title of the document.
     *
     * @return  string
     *
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Adds css file to document
     *
     * @param String $file
     * @param null $attributes
     * @return $this
     */
    public function addCss(String $file, Array $attributes = null)
    {
        /* Check, if file is not already in document */
        foreach ($this->styleSheets as $styleSheet) {
            if ($styleSheet == $file)
            {
                return $this;
            }
        }

        $this->styleSheets[] = $file;

        return $this;
    }
}