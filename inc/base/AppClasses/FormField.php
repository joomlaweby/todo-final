<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 05/08/2019
 * Time: 22:25
 */

namespace App\Form;
defined('_APP_EXEC') or die;

class FormField
{
    public $name;
    public $type;
    public $extension;
    public $label;
    public $description;
}