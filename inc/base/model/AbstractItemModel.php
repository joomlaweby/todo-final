<?php
namespace App\Base;
defined('_APP_EXEC') or die;
use Joomla\Database;
use Joomla\Database\DatabaseDriver;
use Joomla\Model\DatabaseModelInterface;
use Joomla\Model\DatabaseModelTrait;


use App;
use Joomla\Registry\Registry;

/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 07/07/2019
 * Time: 21:00
 */

abstract class AbstractItemModel implements DatabaseModelInterface {

    use DatabaseModelTrait;

    public $id;
    public $name;
    public $ordering;
    public $createdAt;
    public $updatedAt;
    public $createdBy;
    public $content;
    public $language;
    public $params;





    /**
     * Get the model state.
     *
     * @return  Registry  The state object.
     *
     * @since   1.0.0
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set the model state.
     *
     * @param   Registry  $state  The state object.
     *
     * @return  void
     *
     * @since   1.0.0
     */
    public function setState(Registry $state)
    {
        $this->state = $state;
    }

    /**
     * Load the model state.
     *
     * @return  Registry  The state object.
     *
     * @since   1.0.0
     */
    protected function loadState()
    {
        return new Registry;
    }

    /**
     * Returns the category parameters
     *
     * @return  Registry
     *
     * @since   1.6
     */
    public function getParams()
    {
        if (!($this->params instanceof Registry))
        {
            $this->params = new Registry($this->params);
        }

        return $this->params;
    }


    function getItem($id) {

        $db = $this->db;
        $table = $this->getDbTableName();

        $query = $db->getQuery(true)
            ->select('*')
            ->from($db->quoteName($table))
            ->where('id', $id)
            ->setLimit(1);

        $result = $db->setQuery($query)->loadObject();

        $this->id = $result->id;
        $this->name = $result->name;
        $this->ordering = $result->ordering;
        $this->createdAt = $result->created_at;
        $this->updatedAt = $result->updated_at;
        $this->createdBy = $result->created_by;

        if (isset($result->alias)) {
            $this->alias = $result->alias;
        }

        return $this;
    }

    function insertItem() {

        $table = $this->getDbTableName();
        $db = $this->db;

        $sql = "SELECT MAX(ordering) as max_ordering FROM $table";

        $result = $db->setQuery($sql)->loadResult();
        $this->id = 0;
        $this->ordering = $result + 1;

        if (isset($this->alias)) {
            $sql = "INSERT INTO $table (id, name, ordering, created_at, published, updated_at, alias, created_by)
            VALUES (0, '$this->name', $this->ordering, NOW(), $this->published, $this->updatedAt, '$this->alias', $this->createdBy)";
        }
        else {
            $sql = "INSERT INTO $table (name, ordering, created_at, published, updated_at, created_by)
            VALUES ('$this->name', $this->ordering, NOW(), $this->published, $this->updatedAt, $this->createdBy)";
        }

        return $db->setQuery($sql)->execute();

    }

    /**
     * Deletes item from database
     *
     * @return  Bool  True on success
     */
    function deleteItem() {
        $db = $this->db;
        $table = $this->getDbTableName();

        if($this->delete() and $this->consolidateItems())
        {
            return true;
        }
        else {
            return false;
        };


    }



    /**
     *  Recalculates ordering for items
     *
     * @return  Bool  True on success
    **/
    protected function consolidateItems() {

        $table = $this->getDbTableName();

        $db = $this->db;
        $query = $db->getQuery(true)
            ->select('*')
            ->from($db->quoteName($table))
            ->order('ordering');

        $result = $db->setQuery($query)->getIterator();
        $defOrder = 1;

        if (count($result) > 0) {
            foreach ($result as $row)
            {
                $query = $db->getQuery(true)
                    ->update($db->quoteName($table))
                    ->set('ordering = ' . $defOrder++ . '')
                    ->where('id', $row->id);

                $db->setQuery($query)->execute();
            }
        }

        return true;
    }


    public function getClassName() {
        $path = explode('\\', __CLASS__);
        return array_pop($path);
    }



    function getAllItems() {

        $model = $this->getModelName();
        $table = $this->getDbTableName();

        $db = $this->db;

        $iterator = $db->setQuery(
            $db->getQuery(true)
                ->select('*')
                ->from($db->quoteName($table))
                ->order('ordering')
        )->loadObjectList();


        // Modify model name to full name
        $model = get_class($this);

        $r = [];
        if (count($iterator) > 0) {
            foreach ($iterator as $row)
            {
                $row = (array)$row;
                $item = new $model($db);
                $item->id = $row['id'];
                $item->name = $row['name'];
                $item->ordering = $row['ordering'];
                $item->published = $row['published'];
                $item->createdAt = $row['created_at'];
                $item->updatedAt = $row['updated_at'];

                /* Added extra fields Todo: include them automatically */
                if (isset($row['alias'])) {
                    $item->alias = $row['alias'];
                }

                if (isset($row['url'])) {
                    $item->url = $row['url'];
                }

                if (isset($row['file'])) {
                    $item->file = $row['file'];
                }

                if (isset($row['category_name'])) {
                    $item->categoryName = $row['category_name'];
                }

                $r[] = $item;
            }
        }

        return $r;
    }

    protected function getModelName() {
        $function = new \ReflectionClass($this);
        return substr($function->getShortName(), 0, -5);
    }

    protected function getDbTableName() {

        $function = new \ReflectionClass($this);

        $table_name = substr($function->getShortName(), 0, -5);

        $array      = preg_split('#([A-Z][^A-Z]*)#', $this->getModelName(), null, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
        $table_name = '';
        foreach ($array as $key => $item) {
            $table_name .= strtolower($item);
            if ($key + 1 != count($array)) {
                $table_name .= '_';
            }
        }


        return '#__' . $table_name;
    }

   function moveUp() {
        $db = $this->db;
        $table = $this->getDbTableName();
        $query = $db->getQuery(true)
           ->select('ordering')
           ->from($db->quoteName($table))
           ->where('id = ' . $this->id)
           ->setLimit(1);
        $ordering = $db->setQuery($query)->loadResult();

        if ($ordering == 1) {
            return true;
        }

       $query = $db->getQuery(true)
           ->update($db->quoteName($table))
           ->set('ordering = ordering + 1')
           ->where('ordering', ($ordering - 1));

        $db->setQuery($query)->execute();

       $query = $db->getQuery(true)
           ->update($db->quoteName($table))
           ->set('ordering = ordering - 1')
           ->where('id = ' . $this->id);

       return $db->setQuery($query)->execute();
    }

    function moveDown() {
        $db = $this->db;
        $table = $this->getDbTableName();

        $query = $db->getQuery(true)
            ->select('ordering')
            ->from($db->quoteName($table))
            ->where('id = ' . $this->id)
            ->setLimit(1);

        $ordering = $db->setQuery($query)->loadResult();

        $query = $db->getQuery(true)
            ->select('MAX(ordering) as max_ordering')
            ->from($db->quoteName($table));
        $ordering_max = $db->setQuery($query)->loadResult();

        if ($ordering == $ordering_max) {
            return true;
        }


        $sql = "UPDATE $table SET ordering = ordering - 1 WHERE ordering = " . ($ordering + 1) . ";";
        $db->setQuery($sql)->execute();
        $sql = "UPDATE $table SET ordering = ordering + 1 WHERE id = $this->id;";

        return $db->setQuery($sql)->execute();
    }

    function updateItem () {
        $db = $this->db;
        $table = $this->getDbTableName();

        $query = $db->getQuery(true)
            ->update($db->quoteName($table))
            ->set(array(('name = ' . $this->name . ''), 'updated_at = NOW()'))
            ->where('id = ' . $this->id);

        return $db->setQuery($query)->execute();
    }


}