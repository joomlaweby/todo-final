<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 12/07/2019
 * Time: 23:40
 */
namespace App\Base\Controller;
defined('_APP_EXEC') or die;
use Joomla\Application\AbstractApplication;
use Joomla\Application;
use Joomla\Controller\AbstractController;
use Joomla\DI\ContainerAwareInterface;
use Joomla\DI\ContainerAwareTrait;
use Joomla\DI\Container;
use Joomla\Renderer\RendererInterface;
use App\Base\Controller;
use Joomla\Input\Input;
use App\View\DefaultHtmlView;
use App\Base\Model;
use App\Cms\Components\Tag\Model\TagModel;
use App;
use App\WebApplication;
use Joomla\String\StringHelper;
use Joomla\Utilities\ArrayHelper;
use Zend\Diactoros\Response\HtmlResponse;

abstract class AbstractAppController extends AbstractController  implements ContainerAwareInterface {

    protected $model_name;
    protected $table_name;
    protected $client;
    protected $db;
    protected $config;
    protected $defaultMethod = 'getallitems';

    /**
     * The application object.
     *
     * @var    Application\AbstractApplication
     * @since  1.0
     */
    private $app;

    /**
     * The input object.
     *
     * @var    Input
     * @since  1.0
     */
    private $input;

    /**
     * The default view for the app
     *
     * @var    string
     * @since  1.0
     */
    protected $defaultView = 'dashboard';
    /**
     * State object to inject into the model
     *
     * @var    \Joomla\Registry\Registry
     * @since  1.0
     */
    protected $modelState = null;

    /**
     * Constructor.
     *
     * @param   Input                $input  The input object.
     * @param   AbstractApplication  $app    The application object.
     */
    function __construct(Input $input, AbstractApplication $app) {

        $this->app = $app;
        $this->input = $input;
        $this->config = $this->app->get('config');
        $this->db = $app->getDb();
        $this->document = $app->getDocument();
    }


    function getItem() {
        $this->isLoggedIn();
        $db = $this->getApp()->getDb();
        $model_name = $this->getModelName();
        $table_name = $this->getTableName();

        $model = new $model_name($db);
        $view_items = $model->getItem($_GET['id']);

        $view = $this->getControllerName() . 'Edit';

        if ($this->client == 'admin') {

            include_once _APP_LOC . '/admin/template/index.php';
        } else {
            include_once _APP_LOC . '/inc/Template/index.php';
        }

        return true;
    }

    function isLoggedIn() {
        $session = $this->getApp()->getSession();

        $loggedIn = $session->get('user.id');
var_dump($loggedIn);

        if (!$loggedIn) {
            WebApplication::alertMessage("You need to login.", "primary");
            header("Location: /user/login");
            exit;
        }

        return true;
    }

    function getAllItems() {
        $db = $this->getApp()->getDb();
        $this->isLoggedIn();

        $model_name = $this->getModelName();
        $table_name = $this->getTableName();

        $model = new $model_name($db);

        $view_items = $model->getAllItems($table_name);

        $view = $this->getViewName();

        if ($this->client == 'admin') {
            include_once _APP_LOC . '/admin/template/index.php';
        } else {
            include_once _APP_LOC . '/inc/Template/index.php';
        }
        return true;
    }

    protected function getViewName() {
        $viewName = $this->getControllerName() . 'List';

        return $viewName;
    }

    protected function getControllerName() {
        $function = new \ReflectionClass($this);
        return substr($function->getShortName(), 0, -10);
    }

    protected function getTableName() {
        $function = new \ReflectionClass($this);
        $table_name = substr($function->getShortName(), 0, -10);
        $table_name = end(explode('\\', $table_name));

        $array      = preg_split('#([A-Z][^A-Z]*)#', $this->getControllerName(), null, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
        $table_name = '';
        foreach ($array as $key => $item) {
            $table_name .= strtolower($item);
            if ($key + 1 != count($array)) {
                $table_name .= '_';
            }
        }
        $config = $this->getConfig();

        $prefix = $config->get('db_prefix');
        return $prefix . $table_name;
    }



    function deleteItem () {
        $this->isLoggedIn();
        $db = $this->getApp()->getDb();
        $model_name = $this->getModelName();
        $table_name = $this->getTableName();

        $model = new $model_name($db);

        $model->id = $_GET['id'];

        if ($model->deleteItem()) {
            WebApplication::alertMessage('Item was successfuly deleted', 'success');
        };



        header("Location: /" . strtolower($this->getControllerName()) . "/getallitems");
        exit;
    }

    function getModelName() {
        return 'App\\Cms\\Components\\' . $this->getControllerName() . '\\Model\\' . ucfirst($this->getControllerName()). 'Model';
    }

    function publishItem () {
        $db = $this->getApp()->getDb();
        $this->isLoggedIn();
        $input = $this->getApp()->input;
        $model_name = $this->getModelName();
        $table_name = $this->getTableName();

        /* Exception from convention - Todo: needs to be solved */

        if ($model_name == 'App\\Cms\\Components\\Menu\\Model\\menuModel') {
            $model_name = 'App\\Cms\\Components\\Menu\\Model\\MenuItemModel';
        }

        $model = new $model_name($db);

        $model->id = $input->get('id');

        if ($model->publishItem()) {
            WebApplication::alertMessage('Item was successfuly published', 'success');
        };

        header("Location: /" . strtolower($this->getControllerName()) . "/getallitems");
        exit;
    }

    function unpublishItem () {
        $db = $this->getApp()->getDb();
        $this->isLoggedIn();

        $model_name = $this->getModelName();
        $table_name = $this->getTableName();

        /* Exception from convention - Todo: needs to be solved */

        if ($model_name == 'App\\Cms\\Components\\Menu\\Model\\menuModel') {
            $model_name = 'App\\Cms\\Components\\Menu\\Model\\MenuItemModel';
        }

        $model = new $model_name($db);

        $model->id = $_GET['id'];

        if ($model->unpublishItem()) {
            WebApplication::alertMessage('Item was successfuly unpublished', 'success');
        };

        header("Location: /" . strtolower($this->getControllerName()) . "/getallitems");
        exit;
    }

    function getAll () {
        $db = $this->getApp()->getDb();
        $this->isLoggedIn();

        $model_name = $this->getModelName();
        $table_name = $this->getTableName();

        $model = new $model_name($db);

        $view_items = $model->getAllItems($this->table_name);

        return $view_items;
    }

    function insertItem() {
        $db = $this->getApp()->getDb();
        $this->isLoggedIn();
        $userId = $this->getApp()->getSession()->get('user.id');

        if (!$this->getApp()->isAdmin()) {
            App\WebApplication::alertMessage('You do not have permissions for this action.', 'warning');
            header('Location: /');
            exit;
        }
        $model_name = $this->getModelName();

        $model = new $model_name($db);
        $model->name = trim($_POST['name']);

        if ($_POST['alias'] != '') {
            $model->alias = trim(createAlias($_POST['alias']));
        }
        else {
            $model->alias = createAlias($model->name);
        }

        if (isset($_POST['category']) and $_POST['category'] != null) {
            $model->category = $_POST['category'];
        }
        else {
            $model->category = 0;
        }



        $model->createdBy = $userId;
        $model->published = 1;
        $model->updatedAt = 0;

        if($model->insertItem(mb_strtolower($this->getControllerName()))) {
            WebApplication::alertMessage($this->getControllerName(). ' ' . $model->name . ' was successfully added.', 'success');
        };

        header('Location: /'. mb_strtolower($this->getControllerName()) . '/getallitems');
        exit;
    }

    /**
     * Get the application object.
     *
     * @return  Application\AbstractApplication  The application object.
     *
     * @since   1.0
     * @throws  \UnexpectedValueException if the application has not been set.
     */
    public function getApplication()
    {
        if ($this->app)
        {
            return $this->app;
        }

        throw new \UnexpectedValueException('Application not set in ' . __CLASS__);
    }

    /**
     * Set the application object.
     *
     * @param   Application\AbstractApplication  $app  The application object.
     *
     * @return  AbstractController  Returns itself to support chaining.
     *
     * @since   1.0
     */
    public function setApplication(Application\AbstractApplication $app)
    {
        $this->app = $app;

        return $this;
    }

    /**
     * Serialize the controller.
     *
     * @return  string  The serialized controller.
     *
     * @since   1.0
     */
    public function serialize()
    {
        return serialize($this->getInput());
    }

    /**
     * Unserialize the controller.
     *
     * @param   string  $input  The serialized controller.
     *
     * @return  AbstractController  Returns itself to support chaining.
     *
     * @since   1.0
     * @throws  \UnexpectedValueException if input is not the right class.
     */
    public function unserialize($input)
    {
        $input = unserialize($input);

        if (!($input instanceof Input))
        {
            throw new \UnexpectedValueException(sprintf('%s would not accept a `%s`.', __METHOD__, gettype($this->input)));
        }

        $this->setInput($input);

        return $this;
    }

    /**
     * Get the DI container.
     *
     * @return  Container
     *
     * @since   1.0
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * Set the DI container.
     *
     * @param   Container  $container  The DI container.
     *
     * @return  $this  Method allows chaining
     *
     * @since   1.0
     */
    public function setContainer(Container $container)
    {
        $this->container = $container;

        return $this;
    }

    /**
     * Execute the controller.
     *
     * This is a generic method to execute and render a view and is not suitable for tasks.
     *
     * @return  void
     *
     * @since   1.0
     * @throws  \RuntimeException
     */
    public function execute()
    {
        // Get the input
        $input = $this->getInput();

        $task = $input->get('task','view');

        // Find method class
        $methodClass = $this->getMethod();

        // Call method
        if ($methodClass != null) {
            $this->$methodClass();
        }


        // Enable browser caching
        // $this->getApplication()->allowCache(true);

        // Do method stuff

        // Set response
        if ($this->view != null) {
            $this->getApplication()->setResponse(new HtmlResponse($this->view->render()));
        }


        return true;
    }

    /**
     * Get the input object.
     *
     * @return  Input  The input object.
     *
     * @since   1.0
     * @throws  \UnexpectedValueException
     */
    public function getInput()
    {
        if ($this->input)
        {
            return $this->input;
        }

        throw new \UnexpectedValueException('Input not set in ' . __CLASS__);
    }

    /**
     * Set the input object.
     *
     * @param   Input  $input  The input object.
     *
     * @return  AbstractController  Returns itself to support chaining.
     *
     * @since   1.0
     */
    public function setInput(Input $input)
    {
        $this->input = $input;

        return $this;
    }

    public function moveUp () {
        $db = $this->getApp()->getDb();
        $this->isLoggedIn();
        $input = $this->getInput();

        $model_name = $this->getModelName();
        $table_name = $this->getTableName();

        $model = new $model_name($db);

        $model->id = $input->get('id');

        $model->moveUp();

        header("Location: /" . strtolower($this->getControllerName()) . "/getallitems");
        exit;
    }

    public function moveDown () {
        $db = $this->getApp()->getDb();
        $this->isLoggedIn();
        $input = $this->getInput();
        $model_name = $this->getModelName();
        $table_name = $this->getTableName();

        $model = new $model_name($db);

        $model->id = $input->get('id');

        $model->moveDown();

        header("Location: /" . strtolower($this->getControllerName()) . "/getallitems");
        exit;
    }

    function updateItem () {
        $db = $this->getApp()->getDb();
        $this->isLoggedIn();
        $input = $this->getInput();
        $model_name = $this->getModelName();
        $table_name = $this->getTableName();

        $model = new $model_name($db);

        $model->id = $_POST['id'];
        $model->name = $_POST['name'];
        $model->updateItem();

        header("Location: /" . strtolower($this->getControllerName()) . "/getallitems");
        exit;
    }

    /**
     * @return AbstractApplication
     */
    function getApp () {
        return $this->app;
    }

    /**
     * @return mixed
     */
    public function getConfig () {
        return $this->getApp()->getContainer()->get('config');
    }

    /**
     * @return mixed
     */
    public function getMethod() {

        if (explode('?', explode('\\', $this->getApp()->url)[2])[0] == '') {
            return $this->defaultMethod;
        }

        return explode('?', explode('\\', $this->getApp()->url)[2])[0];
    }


    /**
     * @return int
     */
    public function getIdFromUrl() {
        return explode('?', explode('\\', $this->getApp()->url)[3])[0];
    }
}