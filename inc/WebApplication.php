<?php
namespace App;
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 30/06/2019
 * Time: 12:58
 */
defined('_APP_EXEC') or die;

use Joomla\Application\AbstractWebApplication;
use Joomla\Database;
use App\DebugBar;
use Joomla\String\StringHelper;
use App\Service;
use Joomla\Utilities\ArrayHelper;
use Joomla\Database\DatabaseDriver;
use Joomla\DI\ContainerAwareInterface;
use App\Base\Model;
use App\Cms\Components\User\Model\UserModel;
use App\Base\Controller;
use App\Base\Controller\AbstractAppController;
use Joomla\Router\Router;
use App;
use Joomla\DI\Container;
use App\Cms\Components\User\Controller\UserController;
use App\Cms\Components\TodoList\Controller\TodoListController;
use App\Cms\Components\Config\Controller\ConfigController;
use Joomla\Router\Route;
use App\DebugBar\JoomlaHttpDriver;
use Joomla\Session\Session;
use App\Base\AppClasses\Document;
use Joomla\DI\ContainerAwareTrait;
use App\Admin\Components\Dashboard\Controller\DashboardController;
use App\Cms\Components\Chuck\Controller\ChuckController;
final class WebApplication extends AbstractWebApplication implements ContainerAwareInterface {
    use App\Base\AppFunctions\ApplicationTrait;

    protected $db;
    public $user;
    public $controller;
    public static $viewData;
    public $url;
    /**
     * Application router
     *
     * @var  Router
     */
    private $router;

    /**
     * Class constructor.
     *
     * @since   1.0
     */
    function __construct (Container $container ) {


        $this->setContainer($container);

        $this->db = $container->get('db');

        // Merge the config into the application
        $this->config = $container->get('config');

        $this->user = $this->getLoggedInUser();
        $this->theme = $this->config->get('theme.default');
        self::$viewData = new \stdClass();
        define('BASE_URL', $this->get('uri.base.full'));
        define('DEFAULT_THEME', BASE_URL . 'themes/' . $this->theme);
        $this->document = new Document;
        $this->setDocument();


        // Run the parent constructor
        parent::__construct();
    }

    /**
     * Method to run the application routines
     *
     * @return  void
     */
    function doExecute()
    {
        try {
//            var_dump($_SESSION);


            $this->expiredSessionCheck();

            if ($this->debugBar)
            {
                /** @var \DebugBar\DataCollector\TimeDataCollector $collector */
                $collector = $this->debugBar['time'];

                $collector->startMeasure('routing');
            }


            // Instantiate the router

            $routes = ArrayHelper::fromObject(json_decode(file_get_contents(JPATH_CONFIGURATION . '/routes.json')));


            /*   $router->addRoute('GET', '/article/:article_id', 'App\\BlogController');


              */

            if ($this->debugBar)
            {
                /** @var \DebugBar\DataCollector\TimeDataCollector $collector */
                $collector = $this->debugBar['time'];

                $collector->stopMeasure('routing');
            }

            if ($this->debugBar)
            {
                /** @var \DebugBar\DataCollector\TimeDataCollector $collector */
                $collector = $this->debugBar['time'];

                $collector->startMeasure('controller');
            }

            /**
             * Todo: Rewrite to use with fetchController method which returns controller object
             */
            $controller = $this->getController();
            $class = 'App\\Cms\\Components\\' . ucfirst(substr($controller['class'], 0, -10)) .'\\Controller\\' . ucfirst($controller['class']);
            $method = $controller['method'];
var_dump($controller);

            $this->url = str_replace('/', '\\', $this->input->server->getString('REQUEST_URI'));

            /* If current url is in route map, use routing */

            if ($this->checkRoutes($routes, $this->url)) {
                $router = new Router($routes);
                $match = $router->parseRoute($this->url);

                if ($match) {
                    $class = 'App\\Cms\\Components\\' . ucfirst(substr(explode('@', $match->getController())[0], 0, -10)) . '\\Controller\\' . ucfirst(explode('@', $match->getController())[0]);
                    $method = explode('@', $match->getController())[1];


                    /**
                    * Use twig rendering and controller->execute() method
                     */
                    if (0 == 0) {
                        $route = $this->router->parseRoute($this->get('uri.route'), $this->input->getMethod());
                        $controller = $this->getContainer()->get($route->getController());

                        $controller->execute();
                    }
                    else {
                        $controller = new $class($this->input, $this);
                        $controller->$method();
                    }


                }
            }

            /* else use old way - /controller/action */
            else {
var_dump($class);
                $controller = new $class($this->input, $this);

                $controller->$method();
            }
            if ($this->debugBar)
            {
                /** @var \DebugBar\DataCollector\TimeDataCollector $collector */
                $collector = $this->debugBar['time'];

                $collector->stopMeasure('controller');
            }

        }
        catch (\Throwable $e) {
            error_log($e);

            header('HTTP/1.1 500 Internal Server Error', null, 500);
            echo '<html><head><title>Application execution error</title></head><body><h1>Application execution error</h1><p>An error occurred while executing app: ' . $e->getMessage() . ' on line ' . $e->getLine() . ' in file ' . $e->getFile() . '</p><pre>' . $e->getTraceAsString() . '</pre></body></html>';

            exit(1);
        }



    }

    function getController () {



        $url = explode('?', substr($this->input->server->getString('REQUEST_URI'), 1))[0];
var_dump($url);
        $segments = explode('/', $url);

        /*
         * If admin as first segment
         */
        if ($segments[0] == 'admin') {


            /*
             * Check if logged in user is Admin
             */
            if (!$this->isAdmin()) {
                $this->alertMessage('You do not have access for this action.', 'warning');
                header('Location: /');
                exit;
            }
            /* Temporary use default controller */


            $class = 'Config' . 'Controller';
            $method = 'get';

        }

        /*
         * Else load controller file from controllers folder
         */

        else if (isset($segments[0]))
        {
var_dump($segments);

            $class = $segments[0] . 'Controller';

            $method = $segments[1];

        } else if
        (
            ((isset($_GET['id'])))
            or (!isset($segments[0]) and (!isset($segments[1])))
            or $segments[0] == ''

        ) {

            /* Use default controller */

            $class = 'Todolist' . 'Controller';
            $method = 'getAllItems';
        }
        else {
            $this->show_404();
        }

        $controller = [
            'class' => $class,
            'method' => $method
        ];

        return $controller;

    }

    static function getView ($view, $viewItems = null, $viewFormData = null) {
        $view_items = $viewItems;
        $view_formdata = $viewFormData;

        if (strpos($view, 'List')) {

            if (strpos($view, 'Item')) {
                $admin_view = strtolower(substr($view, 0, -8));
            }
            else {
                $admin_view = strtolower(substr($view, 0, -4));
            }
        }

        include_once _APP_LOC . '/inc/Template/view/' . $view . 'View.php';
    }



}