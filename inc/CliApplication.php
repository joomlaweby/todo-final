<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 03/08/2019
 * Time: 19:03
 */

namespace App;

use Joomla\Console\Application;

class CliApplication extends Application
{
    public function execute()
    {
        $this->out('Hello World');
    }
}