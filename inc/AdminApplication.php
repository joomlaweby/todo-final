<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 08/08/2019
 * Time: 00:05
 */

namespace App;
defined('_APP_EXEC') or die;

use App\Base\AppClasses\Document;
use Joomla\Application\AbstractWebApplication;
use Joomla\Database;
use App\DebugBar;
use Joomla\String\StringHelper;
use App\Service;
use Joomla\Utilities\ArrayHelper;
use Joomla\Database\DatabaseDriver;
use Joomla\DI\ContainerAwareInterface;
use App\Base\Model;
use App\Cms\Components\User\Model\UserModel;
use App\Base\Controller;
use App\Base\Controller\AbstractAppController;
use Joomla\Router\Router;
use App;
use Joomla\DI\Container;
use App\Cms\Components\User\Controller\UserController;
use App\Cms\Components\TodoList\Controller\TodoListController;
use App\Cms\Components\Config\Controller\ConfigController;
use Joomla\Router\Route;
use App\DebugBar\JoomlaHttpDriver;
use Joomla\Session\Session;
use App\Admin\Components\Modules\Model\ModulesModel;
class AdminApplication extends AbstractWebApplication implements ContainerAwareInterface
{
    use App\Base\AppFunctions\ApplicationTrait;

    protected $db;
    public $user;
    public $controller;
    public static $viewData;

    /**
     * Application router
     *
     * @var  Router
     */
    private $router;



    /**
     * Class constructor.
     *
     * @since   1.0
     */
    function __construct (Container $container ) {


        $this->setContainer($container);

        $this->db = $container->get('db');

        // Merge the config into the application
        $this->config = $container->get('config');

        $this->user = $this->getLoggedInUser();
        $this->theme = $this->config->get('theme.default');
        self::$viewData = new \stdClass();
        define('BASE_URL', $this->get('uri.base.full'));
        define('DEFAULT_THEME', BASE_URL . 'themes/' . $this->theme);
        $this->document = new Document;
        $this->setDocument();
        // Run the parent constructor
        parent::__construct();
    }

    /**
     * Method to run the application routines
     *
     * @return  void
     */
    function doExecute()
    {
        try {

            $this->expiredSessionCheck();

            if ($this->debugBar)
            {
                /** @var \DebugBar\DataCollector\TimeDataCollector $collector */
                $collector = $this->debugBar['time'];

                $collector->startMeasure('routing');
            }


            // Instantiate the router

            $routes = ArrayHelper::fromObject(json_decode(file_get_contents(JPATH_CONFIGURATION . '/routes.json')));


            /*   $router->addRoute('GET', '/article/:article_id', 'App\\BlogController');


              */

            if ($this->debugBar)
            {
                /** @var \DebugBar\DataCollector\TimeDataCollector $collector */
                $collector = $this->debugBar['time'];

                $collector->stopMeasure('routing');
            }

            if ($this->debugBar)
            {
                /** @var \DebugBar\DataCollector\TimeDataCollector $collector */
                $collector = $this->debugBar['time'];

                $collector->startMeasure('controller');
            }

            /**
             * Todo: Rewrite to use with fetchController method which returns controller object
             */
            $controller = $this->getController();

            $class = 'App\\Admin\\Components\\' . StringHelper::ucfirst($controller['component']) .'\\Controller\\' . ucfirst($controller['class']);
            $method = $controller['task'];
            $view = $controller['view'];
            /* If current url is in route map, use routing */

            /**
             * Use twig rendering and controller->execute() method
             */
            if ($class == 'App\Admin\Components\Dashboard\Controller\DashboardController' || $class == 'App\Admin\Components\Modules\Controller\ModulesController') {


                $model = 'App\\Admin\\Components\\' . ucfirst(substr($controller['class'], 0, -10)) . '\\Model\\' . ucfirst(substr($controller['class'], 0, -10)) . 'Model';
                $view = 'App\\Admin\\Components\\' . ucfirst(substr($controller['class'], 0, -10)) .'\\View\\' . ucfirst(substr($controller['class'], 0, -10)) . 'HtmlView';


                $view = new $view($this->getContainer()->get('renderer'), $this->getContainer()->get(AdminApplication::class)->getDocument(),  new $model($this->db));
                $view = $view->setLayout('index.twig');
                $controller = new $class($view, $this->input, $this);
                $controller->execute();
            }
            else {


                if (ucfirst(substr($controller['class'], 0, -10)) == '') {
                    $class = 'App\Cms\Components\Todolist\Controller\TodolistController';
                    $method = 'getallitems';
                }
                $controller = new $class($this->input, $this);
                $controller->$method();
            }


            if ($this->debugBar)
            {
                /** @var \DebugBar\DataCollector\TimeDataCollector $collector */
                $collector = $this->debugBar['time'];

                $collector->stopMeasure('controller');
            }

        }
        catch (\Throwable $e) {
            error_log($e);

            header('HTTP/1.1 500 Internal Server Error', null, 500);
            echo '<html><head><title>Application execution error</title></head><body><h1>Application execution error</h1><p>An error occurred while executing app: ' . $e->getMessage() . ' on line ' . $e->getLine() . ' in file ' . $e->getFile() . '</p><pre>' . $e->getTraceAsString() . '</pre></body></html>';

            exit(1);
        }



    }

    /**
     * @param $view
     * @param null $viewItems
     * @param null $viewFormData
     *
     * @deprecated
     */

    static function getView ($view, $viewItems = null, $viewFormData = null) {
        $view_items = $viewItems;
        $view_formdata = $viewFormData;

        if (strpos($view, 'List')) {

            if (strpos($view, 'Item')) {
                $admin_view = strtolower(substr($view, 0, -8));
            }
            else {
                $admin_view = strtolower(substr($view, 0, -4));
            }
        }

        if (file_exists(_APP_LOC . '/inc/cms/components/***/view/' . $view . 'View.php')) {

        }
        include_once _APP_LOC . '/inc/Template/view/' . $view . 'View.php';
    }

    public function getController () {

        $controller = [
            'component' => $this->getApp()->input->get('component'),
            'class' => $this->getApp()->input->get('component') . 'Controller',
            'view' => StringHelper::ucfirst($this->getApp()->input->get('component')) . StringHelper::ucfirst($this->getApp()->input->get('view')) . 'View',
            'method' => $this->getApp()->input->get('task'),
            'id' => $this->getApp()->input->get('id')
        ];


        return $controller;
    }

}