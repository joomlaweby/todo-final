<?php
/**
 * Joomla! Framework Website
 *
 * @copyright  Copyright (C) 2014 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license    http://www.gnu.org/licenses/gpl-2.0.txt GNU General Public License Version 2 or Later
 */

namespace App\Service;

use App\Admin\Components\Modules\Model\ModulesModel;
use App\Cms\Components\Todolist\Model\TodolistItemModel;
use App\Cms\Components\User\Model\UserModel;
use App\Cms\Components\Blog\Model\BlogModel;
use App\Cms\Components\Blog\Controller\BlogController;
use Joomla\Application as JoomlaApplication;
use App\AdminApplication as AdminApplication;
use Joomla\Console\Application as ConsoleApplication;
use Joomla\Console\Loader\ContainerLoader;
use Joomla\Console\Loader\LoaderInterface;
use Joomla\Database\DatabaseInterface;
use Joomla\DI\Container;
use Joomla\DI\ServiceProviderInterface;
use Joomla\Event\Command\DebugEventDispatcherCommand;
use Joomla\Event\DispatcherInterface;
use App\Command\GenerateSriCommand;
use App\Command\Packagist\DownloadsCommand;
use App\Command\Packagist\SyncCommand as PackagistSyncCommand;
use App\Command\Twig\ResetCacheCommand;
use App\Command\UpdateCommand;
use App\Controller;
use App\Admin\Components\Dashboard\Controller\DashboardController;
use App\Base\Controller\PageController;
use App\Cms\Components\User\Controller\UserController;
use App\Cms\Components\User\View\LoginHtmlView;
use App\Admin\Components\Modules\Controller\ModulesController;
use App\Base\Controller\WrongCmsController;
use App\DebugBar;
use App\DebugBar\JoomlaHttpDriver;
use App\View;
use App\View\DefaultHtmlView;
use App\WebApplication;
use Joomla\FrameworkWebsite\Model\ReleaseModel;
use Joomla\Http\Http;
use Joomla\Input\Input;
use Joomla\Registry\Registry;
use Joomla\Renderer\RendererInterface;
use Joomla\Renderer\TwigRenderer;
use Joomla\Router\Command\DebugRouterCommand;
use Joomla\Router\Route;
use Joomla\Router\Router;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use TheIconic\Tracking\GoogleAnalytics\Analytics;
use App\Admin\Components\Dashboard\View\DashboardHtmlView;
use App\Admin\Components\Modules\View\ModulesHtmlView;
use App\Cms\Components\Todolist\Controller\TodolistController;
use App\Cms\Components\Page\View\PageHtmlView;
use App\Cms\Components\Todolist\View\TodolistHtmlView;
/**
 * Application service provider
 */
class ApplicationProvider implements ServiceProviderInterface
{
	/**
	 * Registers the service provider with a DI container.
	 *
	 * @param   Container  $container  The DI container.
	 *
	 * @return  void
	 */
	public function register(Container $container): void
	{
		/*
		 * Application Classes
		 */

		$container->share(ConsoleApplication::class, [$this, 'getConsoleApplicationService'], true);

		$container->alias(WebApplication::class, JoomlaApplication\AbstractWebApplication::class)
			->share(JoomlaApplication\AbstractWebApplication::class, [$this, 'getWebApplicationClassService'], true);

        $container->alias(AdminApplication::class, AdminApplication::class)
            ->share(AdminApplication::class, [$this, 'getAdminApplicationClassService'], true);
		/*
		 * Application Helpers and Dependencies
		 */

		$container->alias(Analytics::class, 'analytics')
			->share('analytics', [$this, 'getAnalyticsService'], true);

		$container->alias(ContainerLoader::class, LoaderInterface::class)
			->share(LoaderInterface::class, [$this, 'getCommandLoaderService'], true);

		$container->alias(Helper::class, 'application.helper')
			->share('application.helper', [$this, 'getApplicationHelperService'], true);


		$container->alias(Router::class, 'application.router')
			->share('application.router', [$this, 'getApplicationRouterService'], true);

		$container->share(Input::class, [$this, 'getInputClassService'], true);

		/*
		 * Console Commands
		 */

		$container->share(DebugEventDispatcherCommand::class, [$this, 'getDebugEventDispatcherCommandService'], true);
		$container->share(DebugRouterCommand::class, [$this, 'getDebugRouterCommandService'], true);
		$container->share(GenerateSriCommand::class, [$this, 'getGenerateSriCommandService'], true);
		$container->share(ResetCacheCommand::class, [$this, 'getResetCacheCommandService'], true);
		$container->share(UpdateCommand::class, [$this, 'getUpdateCommandService'], true);

		/*
		 * MVC Layer
		 */

		// Controllers


		$container->alias(PageController::class, 'controller.page')
			->share('controller.page', [$this, 'getControllerPageService'], true);


		$container->alias(WrongCmsController::class, 'controller.wrong.cms')
			->share('controller.wrong.cms', [$this, 'getControllerWrongCmsService'], true);

		$container->alias(DashboardController::class, 'controller.dashboard')
				->share('controller.dashboard', [$this, 'getControllerDashboardService'], true);

		$container->alias(UserController::class, 'controller.user')
				->share('controller.user', [$this, 'getControllerUserService'], true);

        $container->alias(ModulesController::class, 'controller.modules')
            ->share('controller.modules', [$this, 'getControllerModulesService'], true);

        $container->alias(TodolistController::class, 'controller.todolist')
            ->share('controller.todolist', [$this, 'getControllerTodolistService'], true);

        $container->alias(BlogController::class, 'controller.blog')
            ->share('controller.blog', [$this, 'getControllerBlogService'], true);

		// Models

        $container->alias(ModulesModel::class, 'model.modules')
            ->share('model.modules', [$this, 'getModelModulesService'], true);

        $container->alias(TodolistModel::class, 'model.todolist')
            ->share('model.todolist', [$this, 'getModelTodolistService'], true);

        $container->alias(UserModel::class, 'model.user')
            ->share('model.user', [$this, 'getModelUserService'], true);

        $container->alias(PageModel::class, 'model.page')
            ->share('model.page', [$this, 'getModelPageService'], true);

        $container->alias(BlogModel::class, 'model.blog')
            ->share('model.blog', [$this, 'getModelBlogService'], true);

		// Views


		$container->alias(DefaultHtmlView::class, 'view.default.html')
			->share('view.default.html', [$this, 'getViewDefaultHtmlService'], true);

        $container->alias(DashboardHtmlView::class, 'view.dashboard.html')
            ->share('view.dashboard.html', [$this, 'getViewDashboardHtmlService'], true);

        $container->alias(ModulesHtmlView::class, 'view.modules.html')
            ->share('view.modules.html', [$this, 'getViewModulesHtmlService'], true);

        $container->alias(TodolistHtmlView::class, 'view.todolist.html')
            ->share('view.todolist.html', [$this, 'getViewTodolistHtmlService'], true);

        $container->alias(LoginHtmlView::class, 'view.login.html')
            ->share('view.login.html', [$this, 'getViewLoginHtmlService'], true);

        $container->alias(PageHtmlView::class, 'view.page.html')
            ->share('view.page.html', [$this, 'getViewPageHtmlService'], true);
	}


	/**
	 * Get the `application.router` service
	 *
	 * @param   Container  $container  The DI container.
	 *
	 * @return  Router
	 */
	public function getApplicationRouterService(Container $container): Router
	{
		$router = new Router;

		/*
		 * CMS Admin Panels
		 */
		$router->get(
			'/administrator',
			WrongCmsController::class
		);

		$router->get(
			'/administrator/*',
			WrongCmsController::class
		);

		$router->get(
			'/wp-admin',
			WrongCmsController::class
		);

		$router->get(
			'/wp-admin/*',
			WrongCmsController::class
		);

		$router->get(
			'wp-login.php',
			WrongCmsController::class
		);

		/*
		 * Web routes
		 */
		$router->addRoute(new Route(['GET', 'HEAD'], '/', PageController::class));
        $router->addRoute(new Route(['GET', 'HEAD', 'POST'], '/user/*', UserController::class));

        /*$router->addRoute(
            new Route(
                ['GET', 'HEAD'],
                '/todolist/getitem/:id',
                TodolistController::class,
                array(
                    'id' => '(\d+)'
                )
            )
        );*/

        $router->addRoute(
            new Route(
                ['POST', 'HEAD', 'GET'],
                '/todolist/*',
                TodolistController::class
            )
        );

        $router->addRoute(
            new Route(
                ['POST', 'HEAD'],
                '/todolist/updateItem',
                TodolistController::class
            )
        );

        $router->addRoute(new Route(['POST', 'HEAD'], '/user/loginSubmit', UserController::class));
		$router->get(
			'/user/getallusers',
			UserController::class
		);

        $router->get(
            '/user/login',
            UserController::class
        );

        $router->get(
            '/dashboard',
            DashboardController::class
        );

        $router->get(
            '/todolist',
            TodolistController::class
        );

        $router->get(
            '/todolist/getallitems',
            TodolistController::class
        );

        $router->get(
            '/todolist/getitem/:id',
            TodolistController::class
        );

        $router->get(
            '/todolist/deleteitem/:id',
            TodolistController::class
        );

        $router->get(
            '/page/getallitems',
            PageController::class
        );

		$router->get(
			'/:view',
			PageController::class
		);

        $router->get(
            '/modules',
            ModulesController::class
        );

        $router->get(
            '/blog',
            BlogController::class
        );

        $router->get(
            '/blog/*',
            BlogController::class
        );

		/*
		 * API routes
		 */
		$router->get(
			'/api/v1/packages',
			StatusControllerGet::class
		);

		$router->get(
			'/api/v1/packages/:package',
			PackageControllerGet::class
		);

		return $router;
	}

	/**
	 * Get the LoaderInterface service
	 *
	 * @param   Container  $container  The DI container.
	 *
	 * @return  LoaderInterface
	 */
	public function getCommandLoaderService(Container $container): LoaderInterface
	{
		$mapping = [
			DebugEventDispatcherCommand::getDefaultName() => DebugEventDispatcherCommand::class,
			DebugRouterCommand::getDefaultName()          => DebugRouterCommand::class,
			GenerateSriCommand::getDefaultName()          => GenerateSriCommand::class,
			ResetCacheCommand::getDefaultName()           => ResetCacheCommand::class,
			UpdateCommand::getDefaultName()               => UpdateCommand::class,
		];

		return new ContainerLoader($container, $mapping);
	}

	/**
	 * Get the ConsoleApplication service
	 *
	 * @param   Container  $container  The DI container.
	 *
	 * @return  ConsoleApplication
	 */
	public function getConsoleApplicationService(Container $container): ConsoleApplication
	{
		$application = new ConsoleApplication(new ArgvInput, new ConsoleOutput, $container->get('config'));

		$application->setCommandLoader($container->get(LoaderInterface::class));
		$application->setDispatcher($container->get(DispatcherInterface::class));
		$application->setLogger($container->get(LoggerInterface::class));
		$application->setName('Joomla! Framework Website');

		return $application;
	}




	/**
	 * Get the `controller.dashboard` service
	 *
	 * @param   Container  $container  The DI container.
	 *
	 * @return  DashboardController
	 */
	public function getControllerDashboardService(Container $container): DashboardController
	{
		return new DashboardController(
			$container->get(Input::class),
			$container->get(WebApplication::class)
		);
	}

    /**
     * Get the `controller.dashboard` service
     *
     * @param   Container  $container  The DI container.
     *
     * @return  BlogController
     */
    public function getControllerBlogService(Container $container): BlogController
    {
        return new BlogController(
            $container->get(Input::class),
            $container->get(WebApplication::class)
        );
    }

    /**
     * Get the `controller.todolist` service
     *
     * @param   Container  $container  The DI container.
     *
     * @return  TodolistController
     */
    public function getControllerTodolistService(Container $container): TodolistController
    {
        return new TodolistController(
            $container->get(Input::class),
            $container->get(WebApplication::class)
        );
    }

    /**
     * Get the `controller.modules` service
     *
     * @param   Container  $container  The DI container.
     *
     * @return  ModulesController
     */
    public function getControllerModulesService(Container $container): ModulesController
    {
        return new ModulesController(
            $container->get(ModulesHtmlView::class),
            $container->get(Input::class),
            $container->get(WebApplication::class)
        );
    }


	/**
	 * Get the `controller.page` service
	 *
	 * @param   Container  $container  The DI container.
	 *
	 * @return  PageController
	 */
	public function getControllerPageService(Container $container): PageController
	{
		return new PageController(
            $container->get(RendererInterface::class),
            $container->get(Input::class),
            $container->get(WebApplication::class)


		);
	}

    /**
     * Get the `controller.user` service
     *
     * @param   Container  $container  The DI container.
     *
     * @return  UserController
     */
    public function getControllerUserService(Container $container): UserController
    {
        return new UserController(

            $container->get(Input::class),
            $container->get(WebApplication::class),
            $container->get(LoginHtmlView::class)
        );
    }


	/**
	 * Get the `controller.wrong.cms` service
	 *
	 * @param   Container  $container  The DI container.
	 *
	 * @return  WrongCmsController
	 */
	public function getControllerWrongCmsService(Container $container): WrongCmsController
	{
		return new WrongCmsController(
			$container->get(Input::class),
			$container->get(WebApplication::class)
		);
	}

	/**
	 * Get the DebugEventDispatcherCommand service
	 *
	 * @param   Container  $container  The DI container.
	 *
	 * @return  DebugEventDispatcherCommand
	 */
	public function getDebugEventDispatcherCommandService(Container $container): DebugEventDispatcherCommand
	{
		return new DebugEventDispatcherCommand(
			$container->get(DispatcherInterface::class)
		);
	}

	/**
	 * Get the DebugRouterCommand service
	 *
	 * @param   Container  $container  The DI container.
	 *
	 * @return  DebugRouterCommand
	 */
	public function getDebugRouterCommandService(Container $container): DebugRouterCommand
	{
		return new DebugRouterCommand(
			$container->get(Router::class)
		);
	}


	/**
	 * Get the GenerateSriCommand service
	 *
	 * @param   Container  $container  The DI container.
	 *
	 * @return  GenerateSriCommand
	 */
	public function getGenerateSriCommandService(Container $container): GenerateSriCommand
	{
		return new GenerateSriCommand;
	}

	/**
	 * Get the Input class service
	 *
	 * @param   Container  $container  The DI container.
	 *
	 * @return  Input
	 */
	public function getInputClassService(Container $container): Input
	{
		return new Input($_REQUEST);
	}


	/**
	 * Get the ResetCacheCommand service
	 *
	 * @param   Container  $container  The DI container.
	 *
	 * @return  ResetCacheCommand
	 */
	public function getResetCacheCommandService(Container $container): ResetCacheCommand
	{
		return new ResetCacheCommand(
			$container->get(TwigRenderer::class),
			$container->get('config')
		);
	}

	/**
	 * Get the UpdateCommand service
	 *
	 * @param   Container  $container  The DI container.
	 *
	 * @return  UpdateCommand
	 */
	public function getUpdateCommandService(Container $container): UpdateCommand
	{
		return new UpdateCommand;
	}

	/**
	 * Get the `view.contributor.html` service
	 *
	 * @param   Container  $container  The DI container.
	 *
	 * @return  DefaultHtmlView
	 */
	public function getViewDefaultHtmlService(Container $container): DefaultHtmlView
	{
		$view = new DefaultHtmlView(
			$container->get('model.default'),
			$container->get('renderer')
		);

		$view->setLayout('default.twig');

		return $view;
	}

    /**
     * Get the `dashboard.html` service
     *
     * @param   Container  $container  The DI container.
     *
     * @return  DashboardHtmlView
     */
    public function getViewDashboardHtmlService(Container $container): DashboardHtmlView
    {
        $view = new DashboardHtmlView(
            $container->get('renderer'),
			$container->get(WebApplication::class)->getDocument()
        );

        $view->setLayout('index.twig');

        return $view;
    }

    /**
     * Get the `modules.html` service
     *
     * @param   Container  $container  The DI container.
     *
     * @return  ModulesHtmlView
     */
    public function getViewModulesHtmlService(Container $container): ModulesHtmlView
    {

        $view = new ModulesHtmlView(
            $container->get('renderer'),
            $container->get(WebApplication::class)->getDocument(),
            new ModulesModel( $container->get(WebApplication::class)->getDb())
        );

        $view->setLayout('modules.index.twig');

        return $view;
    }

    /**
     * Get the `login.html` service
     *
     * @param   Container  $container  The DI container.
     *
     * @return  LoginHtmlView
     */
    public function getViewLoginHtmlService(Container $container): LoginHtmlView
    {

        $view = new LoginHtmlView(
            $container->get('renderer'),
            $container->get(WebApplication::class)->getDocument(),
            new UserModel( $container->get(WebApplication::class)->getDb())
        );

        $view->setLayout('login.index.twig');

        return $view;
    }

    /**
     * Get the `login.html` service
     *
     * @param   Container  $container  The DI container.
     *
     * @return  TodolistHtmlView
     */
    public function getViewTodolistHtmlService(Container $container): TodolistHtmlView
    {

        $view = new TodolistHtmlView(
            $container->get('renderer'),
            new TodolistItemModel( $container->get(WebApplication::class)->getDb())
        );

        $view->setLayout('todolist.index.twig');

        return $view;
    }


    /**
     * Get the `profile.html` service
     *
     * @param   Container  $container  The DI container.
     *
     * @return  ProfileHtmlView
     */
    public function getViewProfileHtmlService(Container $container): ProfileHtmlView
    {

        $view = new ProfileHtmlView(
            $container->get('renderer'),
            $container->get(WebApplication::class)->getDocument(),
            new UserModel( $container->get(WebApplication::class)->getDb())
        );

        $view->setLayout('profile.index.twig');

        return $view;
    }

	/**
	 * Get the WebApplication class service
	 *
	 * @param   Container  $container  The DI container.
	 *
	 * @return  WebApplication
	 */
	public function getWebApplicationClassService(Container $container): WebApplication
	{
		/** @var Registry $config */
		$config = $container->get('config');

		$application              = new WebApplication($container);
		$application->httpVersion = '2';

		// Inject extra services
		$application->setContainer($container);
		$application->setDispatcher($container->get(DispatcherInterface::class));
		$application->setLogger($container->get(LoggerInterface::class));
		$application->setRouter($container->get(Router::class));


		return $application;
	}

    /**
     * Get the AdminApplication class service
     *
     * @param   Container  $container  The DI container.
     *
     * @return  AdminApplication
     */
    public function getAdminApplicationClassService(Container $container): AdminApplication
    {
        /** @var Registry $config */
        $config = $container->get('config');

        $application              = new AdminApplication($container);
        $application->httpVersion = '2';

        // Inject extra services
        $application->setContainer($container);
        $application->setDispatcher($container->get(DispatcherInterface::class));
        $application->setLogger($container->get(LoggerInterface::class));
        $application->setRouter($container->get(Router::class));


        return $application;
    }

    /**
     * Get the `model.release` service
     *
     * @param   Container  $container  The DI container.
     *
     * @return  TodolistItemModel
     */
    public function getModelTodolistItemService(Container $container): TodolistItemModel
    {
        return new TodolistItemModel($container->get(DatabaseInterface::class));
    }
}
