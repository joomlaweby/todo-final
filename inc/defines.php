<?php
/**
 * Created by PhpStorm.
 * User: Miro
 * Date: 26/07/2019
 * Time: 00:21
 */

defined('_APP_EXEC') or die;

define ('_APP_LOC', \dirname(__DIR__));

define('_APP_START', microtime(true));
define('JPATH_ROOT', \dirname(__DIR__));
define('JPATH_TEMPLATES', JPATH_ROOT . '/inc/Templates');
define('JPATH_CONFIGURATION', JPATH_ROOT . '/inc/Config');
define('JPATH_SETUP',         JPATH_ROOT . '/inc/Setup');
define('_APP_COMPONENTS',         JPATH_ROOT . '/inc/cms/components');