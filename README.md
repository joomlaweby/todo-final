## App3000

Simple-sample CMS application build on Joomla Framewok 

#### Features

- articles
- categories
- tags
- users
- todolist
- pages

#### Technologies used

- Joomla Framework 2.0
- Twig template engine
- Bootstrap 4

