<?php


define('_APP_EXEC', 1);

// Fix magic quotes.
@ini_set('magic_quotes_runtime', 0);

error_reporting(E_ALL & ~E_NOTICE);


require_once("inc/Defines.php");
require_once _APP_LOC . '/libraries/vendor/autoload.php';
require_once _APP_LOC . "/libraries/libraries.php";

// Wrap in a try/catch so we can display an error if need be - creating container
try {

    $container = (new Joomla\DI\Container)
        ->registerServiceProvider(new App\Service\ApplicationProvider)
        ->registerServiceProvider(new App\Service\ConfigServiceProvider(JPATH_CONFIGURATION . '/config.json'))
        ->registerServiceProvider(new App\Service\DatabaseServiceProvider)
        ->registerServiceProvider(new App\Service\EventProvider)
        ->registerServiceProvider(new App\Service\HttpProvider)
        ->registerServiceProvider(new App\Service\LoggingProvider)
        ->registerServiceProvider(new Joomla\Preload\Service\PreloadProvider)
        ->registerServiceProvider(new App\Service\TemplatingProvider);

    // Conditionally include the DebugBar service provider based on the app being in debug mode
    if ((bool)$container->get('config')->get('debug', false)) {
        $container->registerServiceProvider(new App\Service\DebugBarProvider);
    }

    // Alias the web logger to the PSR-3 interface as this is the primary logger for the environment
    $container->alias(Monolog\Logger::class, 'monolog.logger.application.web')
        ->alias(Psr\Log\LoggerInterface::class, 'monolog.logger.application.web')
        ->alias('data', 'Joomla\Data\Data');

    // Alias the web application to Joomla's base application class as this is the primary application for the environment
    $container->alias(Joomla\Application\AbstractApplication::class, Joomla\Application\AbstractWebApplication::class);

    // Set error reporting based on config
    $errorReporting = (int)$container->get('config')->get('errorReporting', 0);
    error_reporting($errorReporting);

    // There is a circular dependency in building the HTTP driver while the application is being resolved, so it'll need to be set here for now
    if ($container->has('debug.bar')) {
        /** @var \DebugBar\DebugBar $debugBar */
        $debugBar = $container->get('debug.bar');
        $debugBar->setHttpDriver($container->get('debug.http.driver'));
   }

} catch (\Throwable $e) {
    error_log($e);

    header('HTTP/1.1 500 Internal Server Error', null, 500);
    echo '<html><head><title>Container Initialization Error</title></head><body><h1>Container Initialization Error</h1><p>An error occurred while creating the DI container: ' . $e->getMessage() . ' on line ' . $e->getLine() . ' in file ' . $e->getFile() . '</p><pre>' . $e->getTraceAsString() . '</pre></p></body></html>';

    exit(1);
}

// Wrap in a try/catch so we can display an error if need be - executing app
try {

    $container->get(Joomla\Application\AbstractApplication::class)->execute();


} catch (\Throwable $e) {
    error_log($e);

    header('HTTP/1.1 500 Internal Server Error', null, 500);
    echo '<html><head><title>Application execution error</title></head><body><h1>Application execution error</h1><p>An error occurred while executing app: ' . $e->getMessage() . ' on line ' . $e->getLine() . ' in file ' . $e->getFile() . '</p><pre>' . $e->getTraceAsString() . '</pre></body></html>';

    exit(1);
}



